package com.geeklabs.footmark.sqllite;

import java.util.List;

import android.database.sqlite.SQLiteDatabase;

public abstract class AbstractTrackRepository<T> {

	public abstract void save(T track, SQLiteDatabase db);  
	
	public abstract List<T> getTrackings(SQLiteDatabase db);
	
	public abstract void deleteAllTrackings(SQLiteDatabase db);
	
}
