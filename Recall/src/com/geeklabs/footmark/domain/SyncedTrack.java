package com.geeklabs.footmark.domain;

import java.io.Serializable;

public class SyncedTrack extends CTrack implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((serverTrackId == null) ? 0 : serverTrackId.hashCode());
		result = prime * result + ((trackTime == null) ? 0 : trackTime.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SyncedTrack other = (SyncedTrack) obj;
		if (serverTrackId == null) {
			if (other.serverTrackId != null)
				return false;
		} else if (!serverTrackId.equals(other.serverTrackId))
			return false;
		if (trackTime == null) {
			if (other.trackTime != null)
				return false;
		} else if (!trackTime.equals(other.trackTime))
			return false;
		return true;
	}
	
}
