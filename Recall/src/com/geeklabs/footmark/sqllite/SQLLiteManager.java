package com.geeklabs.footmark.sqllite;

import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.geeklabs.footmark.TrackConverter;
import com.geeklabs.footmark.domain.OfflineTrack;
import com.geeklabs.footmark.domain.SyncedTrack;

public class SQLLiteManager extends SQLiteOpenHelper {

	private static SQLLiteManager mInstance = null;

	private static final String DATABASE_NAME = "Geeklabs_Footmark_db";
	private static final int DATABASE_VERSION = 1;

	private final OfflineTrackRepository offlineTrackRepository = new OfflineTrackRepository();
	private final SyncedTrackRepository syncedTrackRepository = new SyncedTrackRepository();
	
	private SQLLiteManager(Context ctx) {
		super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public static SQLLiteManager getInstance(Context ctx) {
		// Use the application context, which will ensure that you
		// don't accidentally leak an Activity's context.
		if (mInstance == null) {
			mInstance = new SQLLiteManager(ctx.getApplicationContext());
		}
		return mInstance;
	}

	// This method will be called first time when db is created
	@Override
	public void onCreate(SQLiteDatabase db) {
		// Create a Track table
		createOfflineTrackingTable(db);
		createSyncedTrackingTable(db);
	}

	private void createOfflineTrackingTable(SQLiteDatabase db) {
		String tableCreation = "CREATE TABLE IF NOT EXISTS OfflineTrack(" + "id INTEGER PRIMARY KEY AUTOINCREMENT," + "lat DOUBLE NOT NULL, " + "lng DOUBLE NOT NULL, " + "date LONG, "
				+ "content VARCHAR(255), " + "tag VARCHAR(15), " + "isFavorite BOOLEAN, " + "isDeleted BOOLEAN, " + "serverTrackId LONG, capturedImage BLOB)";
		db.execSQL(tableCreation);
	}

	private void createSyncedTrackingTable(SQLiteDatabase db) {
		String tableCreation = "CREATE TABLE IF NOT EXISTS SyncedTrack(" + "id INTEGER PRIMARY KEY AUTOINCREMENT," + "lat DOUBLE NOT NULL, " + "lng DOUBLE NOT NULL, " + "date LONG, "
				+ "content VARCHAR(255), " + "tag VARCHAR(15), " + "isFavorite BOOLEAN, " + "serverTrackId LONG, address VARCHAR(255), capturedImage BLOB)";
		db.execSQL(tableCreation);
	}
	
	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		// Create tables again
		onCreate(db);
	}

	public void saveOfflineTrack(OfflineTrack offlineTrack) {
		SQLiteDatabase db = this.getWritableDatabase();
		offlineTrackRepository.save(offlineTrack, db);
	}
	
	public void saveSyncedTrack(SyncedTrack syncedTrack) {
		SQLiteDatabase db = this.getWritableDatabase();
		syncedTrackRepository.save(syncedTrack, db);
		Log.i("Save Sync Track", "Successfully Saved!");
	}

	public List<SyncedTrack> getAllSyncedTrackings() {
		return syncedTrackRepository.getTrackings(getWritableDatabase());
	}

	public List<OfflineTrack> getAllOfflineTrackings() {
		return offlineTrackRepository.getTrackings(getWritableDatabase());
	}

	public void deleteAllSyncedTrackings() {
		syncedTrackRepository.deleteAllTrackings(getWritableDatabase());
		Log.i("Deleted Track", "Successfully Deleted all Sync Tracks !");
	}

	public void deleteAllOfflineTrackings() {
		offlineTrackRepository.deleteAllTrackings(getWritableDatabase());
		Log.i("Deleted Track", "Successfully Deleted all offline Tracks! ");
	}

	public boolean isServerTrackExistInOfflineTrack(Long trackId) {
		return offlineTrackRepository.isServerTrackExist(trackId, getWritableDatabase());
	}

	public void updateSyncedTrackAsFavorite(SyncedTrack track) {
		syncedTrackRepository.updateTrackAsFavorite(track, getWritableDatabase());
		saveOfflineTrack(TrackConverter.convertToOfflineTrack(track));
		Log.i("Update  Track", "Successfully Done Update on Sync and saved offline!");
	}

	public void updateOfflineTrackAsFavorite(OfflineTrack track) {
		offlineTrackRepository.updateOfflineTrackAsFavorite(track, getWritableDatabase());
		Log.i("Update  Track", "Successfully Done Update saved offline!");
	}
	
	public void deleteSelectedOfflineTrack(OfflineTrack track) {
		offlineTrackRepository.deleteSelectedOfflineTrack(track, getWritableDatabase());
		Log.i("Delete  Track", "Successfully Done Delete on Sync and saved offline!");
	}
	
	public void deleteSelectedSyncedTrack(SyncedTrack track) {
		syncedTrackRepository.deleteSelectedTrack(track, getWritableDatabase());
		OfflineTrack offlineTrack = TrackConverter.convertToOfflineTrack(track);
		offlineTrack.setDeleted(true);
		
		saveOfflineTrack(offlineTrack);
		Log.i("Delete  Track", "Successfully Done Deleted on Sync and saved offline!");
	}

	public void updateSyncedTrack(SyncedTrack track) {
		syncedTrackRepository.updateSyncedTrack(track, getWritableDatabase());
		saveOfflineTrack(TrackConverter.convertToOfflineTrack(track));
		Log.i("Update  Track", "Successfully Done Update on Sync and saved offline!");
	}
	
	public void updateOfflineTrack(OfflineTrack track) {
		offlineTrackRepository.updateOfflineTrack(track, getWritableDatabase());
		Log.i("Update  Track", "Successfully Done Update on offline!");
	}
	
	public List<SyncedTrack> getPaginationSyncedTracks(int currentPage) {
		return syncedTrackRepository.getPaginationSyncedTracks(currentPage, getWritableDatabase());
	}

	public List<OfflineTrack> getPaginationOfflineTracks(int currentPage) {
		return offlineTrackRepository.getPaginationOfflineTracks(currentPage, getWritableDatabase());
	}
	
}