package com.geeklabs.footmark.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class LocationPreferences {

	private static final String LAST_TRACKED_LAT = "0.0";
	private static final String LAST_TRACKED_LNG = "0.0";
	private static final String LAST_KNOWN_LAT = "0.0";
	private static final String LAST_KNOWN_LNG = "0.0";

	private SharedPreferences preferences;
	
	public LocationPreferences(Context context) {
		preferences = context.getSharedPreferences("footmark_user_location_pref", Context.MODE_PRIVATE);
	}
	
	//set Locations here 
	
	public void setLastTackedLat(String lastTrackedLat) {
		Editor editor = preferences.edit();
		editor.putString(LAST_TRACKED_LAT, lastTrackedLat);
		editor.commit();
	}
	
	public String getLastTrackedLat(){
		return preferences.getString(LAST_TRACKED_LAT, null);
	}
	
	
	public void setLastTackedLng(String lastTrackedLng) {
		Editor editor = preferences.edit();
		editor.putString(LAST_TRACKED_LNG, lastTrackedLng);
		editor.commit();
	}
	
	public String getLastTrackedLng(){
		return preferences.getString(LAST_TRACKED_LAT, null);
	}
	
	
	public void setLastKnownLat(String lastKnownLat) {
		Editor editor = preferences.edit();
		editor.putString(LAST_KNOWN_LAT, lastKnownLat);
		editor.commit();
	}
	
	public String getLastKnownLat(){
		return preferences.getString(LAST_KNOWN_LAT, null);
	}
	
	
	public void setLastKnownLng(String lastKnownLng) {
		Editor editor = preferences.edit();
		editor.putString(LAST_KNOWN_LNG, lastKnownLng);
		editor.commit();
	}
	
	public String getLastKnownLng(){
		return preferences.getString(LAST_KNOWN_LNG, null);
	}
}
