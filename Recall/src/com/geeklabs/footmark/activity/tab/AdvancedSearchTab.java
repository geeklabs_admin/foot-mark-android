package com.geeklabs.footmark.activity.tab;

import java.util.Calendar;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.geeklabs.footmark.constants.FootMarkConstants;
import com.geeklabs.footmark.util.TimeAndDateUtil;

public class AdvancedSearchTab extends Fragment implements
TextWatcher {
	private Button fromTime_Button, toDate_Button, toTime_Button,
			fromDate_Button;
	private int toHour, toMinute, fromMinute, toYear, fromYear, toMonth,
			fromMonth, toDay, fromDay, fromHour, currentYear, currentMonth,
			currentDay, currentHour, currentMin, toDay1, toMonth1, toYear1,
			fromDay1, fromMonth1, fromYear1;
	private Calendar currentDate, selectedDate;
	private long diffInMillisec = 0;
	private int checkStatus = 0, checkStatus1 = 0;
	private String dayStatus;
	private AutoCompleteTextView autotext1, autotext2;

	private static final int TIME_DIALOG_FROM_ID = 1;
	private static final int DATE_DIALOG_FROM_ID = 2;
	private static final int TIME_DIALOG_TO_ID = 3;
	private static final int DATE_DIALOG_TO_ID = 4;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addDateAndTimeListener();
		//autotext1 = (AutoCompleteTextView) getView().findViewById(R.id.countryNameEditText);
		//autotext2 = (AutoCompleteTextView) getView().findViewById(R.id.state_editText);
		
		autotext1.addTextChangedListener(this);
		autotext2.addTextChangedListener(this);

		// Auto drop down for countries
		autotext1.setAdapter(new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, FootMarkConstants.COUNTRY_NAMES));
	}
	
	public void addDateAndTimeListener() {
		final Calendar calendar = Calendar.getInstance();
		
		toYear = calendar.get(Calendar.YEAR);
		toMonth = calendar.get(Calendar.MONTH);
		toDay = calendar.get(Calendar.DAY_OF_MONTH);

		fromYear = calendar.get(Calendar.YEAR);
		fromMonth = calendar.get(Calendar.MONTH);
		fromDay = calendar.get(Calendar.DAY_OF_MONTH);

		toHour = TimeAndDateUtil.getHourIn12Format(calendar.get(Calendar.HOUR_OF_DAY));
		toMinute = calendar.get(Calendar.MINUTE);

		fromHour = TimeAndDateUtil.getHourIn12Format(calendar.get(Calendar.HOUR_OF_DAY));
		fromMinute = calendar.get(Calendar.MINUTE);

		dayStatus = TimeAndDateUtil.getAMPM(Calendar.HOUR_OF_DAY);

		//fromTime_Button = (Button) getView().findViewById(R.id.fromTime);
		//toTime_Button = (Button) getView().findViewById(R.id.toTime);

		//fromDate_Button = (Button) getView().findViewById(R.id.fromDate);
		//toDate_Button = (Button) getView().findViewById(R.id.toDate);

		fromTime_Button.setText(new StringBuilder().append(padding_str(12))
				.append(":").append(padding_str(00)).append("am"));
		toTime_Button.setText(new StringBuilder().append(padding_str(toHour))
				.append(":").append(padding_str(toMinute)).append(dayStatus));

		fromDate_Button
				.setText(new StringBuilder().append(fromDay).append("-")
						.append(fromMonth + 1).append("-").append(fromYear)
						.append(" "));
		toDate_Button.setText(new StringBuilder().append(toDay).append("-")
				.append(toMonth + 1).append("-").append(toYear).append(" "));

		fromTime_Button.setOnClickListener(new OnClickListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {

				//showDialog(TIME_DIALOG_FROM_ID);
			}
		});
		toTime_Button.setOnClickListener(new OnClickListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {

				//showDialog(TIME_DIALOG_TO_ID);
			}
		});

		fromDate_Button.setOnClickListener(new OnClickListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {

				//showDialog(DATE_DIALOG_FROM_ID);

			}
		});
		toDate_Button.setOnClickListener(new OnClickListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {

				//showDialog(DATE_DIALOG_TO_ID);
			}
		});
	}
	
	private static String padding_str(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}

	@Override
	public void afterTextChanged(Editable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub
		
	}
}
