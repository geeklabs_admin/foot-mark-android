package com.geeklabs.footmark.activity.tab;

import static com.geeklabs.footmark.util.MapUtil.setupStrictMode;

import java.util.List;

import android.R;
import android.app.Dialog;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.geeklabs.footmark.communication.get.task.GetTracksTask;
import com.geeklabs.footmark.domain.SyncedTrack;
import com.geeklabs.footmark.util.MapUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapFragmentTab extends SupportMapFragment {
	private GoogleMap googleMap;

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		setHasOptionsMenu(true);

		// Setup strict mode policies to detect problems while app is running
		setupStrictMode();

		int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
		 
        // Showing status
        if(status == ConnectionResult.SUCCESS) {
			// create Google map instance
			initGoogleMap();
			
			// listenOnMyLocationChange - only for first time
			listenOnMyLocationChange();	
			
			// Remove it -- TODO - adding temp my location
			MapUtil.animateMapToPosition(googleMap, new LatLng(17.4223758, 78.5457027));
        } else {
        	int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, getActivity(), requestCode);
            dialog.show();
        }
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
	}

	@Override
	public void onResume() {
		super.onResume();
		int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
		 // Showing status
        if(status == ConnectionResult.SUCCESS) {
			// create Google map instance
			initGoogleMap();
			
			// listenOnMyLocationChange - only for first time
			listenOnMyLocationChange();	
			
			// Remove it -- TODO - adding temp my location
			MapUtil.animateMapToPosition(googleMap, new LatLng(17.4223758, 78.5457027));
        } else {
        	int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, getActivity(), requestCode);
            dialog.show();
        }
		
	}
	
	// To initialize default map
	private void initGoogleMap() {
		// Do a null check to confirm that we have not already instantiated the
		// map.
		if (googleMap == null) {
			android.support.v4.app.FragmentManager fragmentManager = getFragmentManager();
			Fragment mapFragment = null;//fragmentManager.findFragmentById(R.id.map);
			if (mapFragment != null) {
				googleMap = ((SupportMapFragment) mapFragment).getMap();
			}

			// Check if we were successful in obtaining the map.
			if (googleMap != null) {
				// It shows current location marker
				googleMap.setMyLocationEnabled(true);
				
				// put 16 as default zoom level 
				MapUtil.animateMapToZoom(googleMap, 16);
				
				UiSettings uiSettings = googleMap.getUiSettings();
				uiSettings.setMyLocationButtonEnabled(true);
				uiSettings.setRotateGesturesEnabled(true);
				
				// get tracks in background async task
        		GetTracksTask validateUserRunnable = new GetTracksTask(0, this.getActivity(), null) {
        			@Override
        			protected void updateUI(final List<SyncedTrack> tracks) {
        				getActivity().runOnUiThread(new Runnable() {
        					public void run() {
        						for (SyncedTrack track : tracks) {
        							MarkerOptions markerOptions = new MarkerOptions()
	        							/*.position(new LatLng(track.getLatitude(), track.getLongitude()))
	        							.icon(BitmapDescriptorFactory.fromResource(R.drawable.user_icon))*/
	        							// .anchor(0.5f, 1)
	        							.draggable(true)
	        							.visible(true);
        							googleMap.addMarker(markerOptions);
        						}
        					}
        				});
        			}
        		};
        		validateUserRunnable.execute();
			}
		}
	}

	private void listenOnMyLocationChange() {
		googleMap.setOnMyLocationChangeListener(new OnMyLocationChangeListener() {

			@Override
			public void onMyLocationChange(Location location) {
				if (location != null) {
					MapUtil.animateMapToPositionAndZoom(googleMap,
							new LatLng(location.getLatitude(), location.getLongitude()), 16);
				}
				// for first time when map opens move camera to user
				// location and never listen on this event, set it to
				// null
				//googleMap.setOnMyLocationChangeListener(null);
			}
		});
	}
}