package com.geeklabs.footmark.sqllite;

public class SQLLite {
	public static final String OFFLINE_TRACK_TABLE_NAME = "OfflineTrack";
	public static final String SYNCED_TRACK_TABLE_NAME = "SyncedTrack";
	
	public static final int limit = 3;
}
