package com.geeklabs.footmark.communication.post.task;

import java.io.IOException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;
import android.widget.Toast;

import com.geeklabs.footmark.communication.AbstractHttpPostTask;
import com.geeklabs.footmark.domain.Preferences;
import com.geeklabs.footmark.preferences.AuthPreferences;
import com.geeklabs.footmark.preferences.UserPreferences;
import com.geeklabs.footmark.util.RequestUrl;

public class PreferencesTask extends AbstractHttpPostTask {

	private UserPreferences userPreferences;
	private AuthPreferences authPreferences;
	private Activity contextActivity;
	private Preferences preferences;

	public PreferencesTask(Activity context, ProgressDialog progressDialog, Preferences preferences) {
		super(progressDialog, context);
		this.contextActivity = context;
		this.preferences = preferences;
		userPreferences = new UserPreferences(context);
		authPreferences = new AuthPreferences(context);
	}

	@Override
	protected String getRequestUrl() {
		return RequestUrl.USER_PREFERENCES.replace("{id}", String.valueOf(authPreferences.getUserId()));
	}

	@Override
	protected void showMessageOnUI(final String message) {
		contextActivity.runOnUiThread(new Runnable() {
			public void run() {
				Toast.makeText(contextActivity, message, Toast.LENGTH_SHORT).show();
				Log.i("FM preferences saving", message);
			}
		});
	}

	@Override
	protected String getRequestJSON() {
		// Set user pref id if exist.
		preferences.setId(userPreferences.getId());
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(preferences);
		} catch (Exception e){
			throw new IllegalStateException("problem occured json parsing while sending tracks");
		} 
	}
	
	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		// response is success, then remove tracks from db 
		ObjectMapper mapper = new ObjectMapper();
		try {
			Preferences pref = mapper.readValue(jsonResponse.toString(), Preferences.class);
			String status = pref.getStatus();
			
			if ("success".equals(status)) {
				userPreferences.setDistance(pref.getDistance());
				userPreferences.setTime(pref.getTime());
				userPreferences.setId(pref.getId());
				showMessageOnUI("Preferences successfully updated");
			}else{
				showMessageOnUI("Problem occurued while updating, try after sometime");
			}
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			cancelDialog();
		}
	}
}
