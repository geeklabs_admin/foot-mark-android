package com.geeklabs.footmark.activity;

import android.app.Activity;
import android.location.LocationManager;
import android.os.Bundle;

public class LocationService extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		getLocationManager();
	}

	public  LocationManager getLocationManager() {
		LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		return locationManager;
	}
}
