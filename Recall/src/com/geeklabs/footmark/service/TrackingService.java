package com.geeklabs.footmark.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.geeklabs.footmark.domain.OfflineTrack;
import com.geeklabs.footmark.preferences.TrackAlgPreferences;
import com.geeklabs.footmark.sqllite.SQLLiteManager;
import com.geeklabs.footmark.util.LocationServices;
import com.geeklabs.footmark.util.TrackingServiceManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

public class TrackingService extends Service implements LocationListener, ConnectionCallbacks, OnConnectionFailedListener {

	private final IBinder mBinder = new UserLocationBinder();

	private static final String LOG_TAG = "TrackingService";

	// A request to connect to Location Services
	private LocationRequest mLocationRequest;

	// Stores the current instantiation of the location client in this object
	private LocationClient mLocationClient;

	private Location location;

	// User Current locations
	private double currentLat;
	private double currentLng;

	private File locationFile;

	private TrackAlgPreferences locationPreferences;

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		if (!TrackingServiceManager.canContinue(getApplicationContext())) {
			Log.w(LOG_TAG, "Trying to start service, but no user logged in the app");
			return Service.START_NOT_STICKY; // Don't do anything as user not
												// signed in
		}

		Log.i(LOG_TAG, "Tracking is called");

		// Create Location req and Location client
		setUpLocationClientIfNeeded();

		// Connect the client.
		if (!mLocationClient.isConnected()) {
			mLocationClient.connect();
		}

		// Call track algorithm
		track();

		return Service.START_NOT_STICKY;
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return mBinder;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		// userPreferences = new UserPreferences(getApplicationContext());

		locationPreferences = new TrackAlgPreferences(getApplicationContext());
		// Create db if not exist
		SQLLiteManager.getInstance(getApplicationContext());

		String exStoragePath = Environment.getExternalStorageDirectory().getAbsolutePath();
		// File appTmpPath = new File(exStoragePath + File.separator
		// +"Footmark");
		locationFile = new File(exStoragePath, "fmLogs.txt");
		if (!locationFile.exists()) {
			try {
				// deleteDirectory(appTmpPath);
				// appTmpPath.mkdir();
				locationFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// If the client is connected
		if (mLocationClient != null && mLocationClient.isConnected()) {
			/*
			 * Remove location updates for a listener. The current Activity is
			 * the listener, so the argument is "this".
			 */
			stopPeriodicUpdates();
			mLocationClient.disconnect();
		}
	}

	/**
	 * In response to a request to stop updates, send a request to Location
	 * Services
	 */
	private void stopPeriodicUpdates() {
		mLocationClient.removeLocationUpdates(this);
	}

	/**
	 * In response to a request to start updates, send a request to Location
	 * Services
	 */
	private void startPeriodicUpdates() {
		mLocationClient.requestLocationUpdates(mLocationRequest, this);
	}

	// Track implementation
	public void track() {
		// Need to calculate distance between last KNOWN location and current  location to known the boundary distance 
		//that is we need to know user is in boundary distance after 3 mins or not boundary distance is 100 meters, so after 3 mins user should exist
		 //with in 100 meters
		BufferedWriter fileOutputStream = null;
		try {

			fileOutputStream = new BufferedWriter(new FileWriter(locationFile, true));
			if (fileOutputStream != null) {

				fileOutputStream.write("Start - track method called");
				fileOutputStream.newLine();

				fileOutputStream.write("Track Alg has been called at:" + new Date());
				fileOutputStream.newLine();
				fileOutputStream.flush();

				if (currentLat > 0.0 && currentLng > 0.0) {

					double lastKnownLat = Double.parseDouble(locationPreferences.getLastKnownLat());
					double lastKnownLng = Double.parseDouble(locationPreferences.getLastKnownLng());
					
					double lastTrackedLat = Double.parseDouble(locationPreferences.getLastTrackedLat());
					double lastTrackedLng = Double.parseDouble(locationPreferences.getLastTrackedLng());
					
					Toast.makeText(getApplicationContext(), String.valueOf(currentLat) + "  " + String.valueOf(currentLng), Toast.LENGTH_LONG).show();
					float boundaryDistance[] = new float[1];
					Location.distanceBetween(lastKnownLat, lastKnownLng, currentLat, currentLng, boundaryDistance);

					fileOutputStream.write("boundary distance:" + boundaryDistance[0]);
					fileOutputStream.newLine();
					
					// Need to calculate distance between last TRACKED location and current location to known the actual distance user  travelled
					// from last tracked location. that is we need to know user is travelled at-least 1 km, then
					// only eligible for tracking actal distance is 1000 meters, so after 3 mins user should be travel at-least 1000 meters
					float actualDistance[] = new float[1];
					Location.distanceBetween(lastTrackedLat, lastTrackedLng, currentLat, currentLng, actualDistance);

					fileOutputStream.write("actual Distance:" + actualDistance[0]);
					fileOutputStream.newLine();
					
					fileOutputStream.write("currentLat:" + currentLat + " currentLng:" + currentLng);
					fileOutputStream.newLine();

					fileOutputStream.write("lastKnownLat:" + lastKnownLat + " lastKnownLng:" + lastKnownLng);
					fileOutputStream.newLine();
					fileOutputStream.write("lastTrackedLat:" + lastTrackedLat + " lastTrackedLng:" + lastTrackedLng);
					fileOutputStream.newLine();

					// write distance in file
					String distance = "in Track method: distances: boundaryDistance: " + String.valueOf(boundaryDistance[0]) + ", actual Distance:  " + String.valueOf(actualDistance[0]);
					fileOutputStream.write(distance);
					fileOutputStream.newLine();
					fileOutputStream.flush();

					if (boundaryDistance[0] <= 500 && actualDistance[0] >= 3000) {
						// saveTrack(); // Save track in SQL Lite

						Toast.makeText(getApplicationContext(), String.valueOf(boundaryDistance[0]) + "  " + String.valueOf(actualDistance[0]), Toast.LENGTH_LONG).show();

						saveTrack(boundaryDistance[0], actualDistance[0]);

						lastTrackedLat = currentLat;
						lastTrackedLng = currentLng;
						fileOutputStream.write("Successfully tracked");
						
						//set last tracked in preferences
						locationPreferences.setLastTrackedLat(String.valueOf(lastTrackedLat));
						locationPreferences.setLastTrackedLng(String.valueOf(lastTrackedLng));
						
						Toast.makeText(getApplicationContext(), "last Tracked location:" + String.valueOf(lastTrackedLat) + "  " + String.valueOf(lastTrackedLng), Toast.LENGTH_LONG).show();
					}

					// Reset Last known location to current location after every 3 minutes.
					lastKnownLat = currentLat;
					lastKnownLng = currentLng;
					
					locationPreferences.setLastKnownLat(String.valueOf(lastKnownLat));
					locationPreferences.setLastKnownLng(String.valueOf(lastKnownLng));
					
					Toast.makeText(getApplicationContext(), "last known:" + String.valueOf(lastKnownLat) + "  " + String.valueOf(lastKnownLng), Toast.LENGTH_LONG).show();
				}

				fileOutputStream.write("End track method");
				fileOutputStream.newLine();
				fileOutputStream.flush();
			}
		} catch (FileNotFoundException e2) {
			e2.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void saveTrack(float boundaryDistance, float actualDistance) {
		GregorianCalendar calendar = new GregorianCalendar(TimeZone.getTimeZone("UTC"));

		// here convert date into long value
		Date date = calendar.getTime();

		OfflineTrack track = new OfflineTrack();
		// set currentLat, currentLng, date to Track Obj
		track.setLatitude(currentLat);
		track.setLongitude(currentLng);
		track.setTrackTime(date);

		SQLLiteManager.getInstance(getApplicationContext()).saveOfflineTrack(track);

		// write data to a file
		String savedTrack = "Succeffully tracked:" + String.valueOf(currentLat) + "  " + String.valueOf(currentLng) + " on " + String.valueOf(date) + " at Act Distance:"
				+ String.valueOf(actualDistance) + "& bondary dis: " + String.valueOf(boundaryDistance);
		FileOutputStream fileOutputStream;

		try {
			fileOutputStream = new FileOutputStream(locationFile, true);
			if (fileOutputStream != null) {
				fileOutputStream.write(savedTrack.getBytes());
				fileOutputStream.flush();
			}
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/*
	  private void saveTrack() { GregorianCalendar calendar = new
	  GregorianCalendar(TimeZone.getTimeZone("UTC"));
	  
	  // here convert date into long value Date date = calendar.getTime();
	  
	  track = new Track(); //set currentLat, currentLng, date to Track Obj
	  track.setLatitude(currentLat); track.setLongitude(currentLng);
	  track.setTrackTime(date);
	  
	  SQLLiteManager.getInstance(getApplicationContext()).saveTrack(track); }
	 */
	private void setUpLocationClientIfNeeded() {
		createLocReq();
		if (mLocationClient == null) {
			mLocationClient = new LocationClient(getApplicationContext(), this, // ConnectionCallbacks
					this); // OnConnectionFailedListener
		}
	}

	private void createLocReq() {
		if (mLocationRequest == null) {
			// Create a new global location parameters object
			mLocationRequest = LocationRequest.create();
			// Set the update interval
			mLocationRequest.setInterval(LocationServices.UPDATE_INTERVAL_IN_MILLISECONDS);
			// Use high accuracy
			mLocationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
			// Set the interval ceiling to one minute
			mLocationRequest.setFastestInterval(LocationServices.FAST_INTERVAL_CEILING_IN_MILLISECONDS);
		}
	}

	public void updateLocation(Location location) {
		currentLat = location.getLatitude();
		currentLng = location.getLongitude();
	}

	@Override
	public void onLocationChanged(Location location) {
		if (location != null) {
			this.location = location;
			updateLocation(location);
		}
	}

	public Location getLocation() {
		return location;
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		// TODO
		/*
		 * Google Play services can resolve some errors it detects. If the error
		 * has a resolution, try sending an Intent to start a Google Play
		 * services activity that can resolve error.
		 */
		// throw new
		// IllegalStateException("problem while connecting to google paly services");
	}

	@Override
	public void onConnected(Bundle arg0) {
		startPeriodicUpdates();
		Location location = mLocationClient.getLastLocation();
		if (location != null) {
			onLocationChanged(location);
		}
	}

	@Override
	public void onDisconnected() {
	}

	public class UserLocationBinder extends Binder {
		public TrackingService getService() {
			return TrackingService.this;
		}
	}

	public static boolean deleteDirectory(File path) {
		if (path.exists()) {
			File[] files = path.listFiles();
			if (files == null) {
				return true;
			}
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					deleteDirectory(files[i]);
				} else {
					files[i].delete();
				}
			}
		}
		return (path.delete());
	}
}
