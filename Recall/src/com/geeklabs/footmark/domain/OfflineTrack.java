package com.geeklabs.footmark.domain;

import java.io.Serializable;

public class OfflineTrack extends CTrack implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((trackTime == null) ? 0 : trackTime.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SyncedTrack other = (SyncedTrack) obj;
		if (id != other.getId())
			return false;
		if (trackTime == null) {
			if (other.getTrackTime() != null)
				return false;
		} else if (!trackTime.equals(other.getTrackTime()))
			return false;
		return true;
	}
	
}
