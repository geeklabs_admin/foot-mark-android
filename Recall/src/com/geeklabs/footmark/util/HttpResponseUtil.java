package com.geeklabs.footmark.util;

import java.io.File;

import android.annotation.TargetApi;
import android.net.http.HttpResponseCache;
import android.os.Build;
import android.util.Log;

public class HttpResponseUtil {

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	public static void stopHttpResponseCache() {
		int currentapiVersion = android.os.Build.VERSION.SDK_INT;
		if (currentapiVersion >= android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH) {			
			HttpResponseCache cache = HttpResponseCache.getInstalled();
			if (cache != null) {
				cache.flush();
			}
		} else if (currentapiVersion >= android.os.Build.VERSION_CODES.GINGERBREAD_MR1) {
			com.integralblue.httpresponsecache.HttpResponseCache installed = com.integralblue.httpresponsecache.HttpResponseCache.getInstalled();
			if (installed != null) {
				installed.flush();
			}
		}
	}

	/**
	 * Implemented with help of https://github.com/candrews/HttpResponseCache
	 */
	public static void enableHttpResponseCache(File cacheDir) {
		final long httpCacheSize = 10 * 1024 * 1024; // 10 MiB
		final File httpCacheDir = new File(cacheDir, "http");
		try {
		    Class.forName("android.net.http.HttpResponseCache")
		        .getMethod("install", File.class, long.class)
		        .invoke(null, httpCacheDir, httpCacheSize);
		} catch (Exception httpResponseCacheNotAvailable) {
		    Log.w("HttpResponseCache Issue", "android.net.http.HttpResponseCache not available, probably because we're running on a pre-ICS version of Android. Using com.integralblue.httpresponsecache.HttpHttpResponseCache.");
		    try{
		        com.integralblue.httpresponsecache.HttpResponseCache.install(httpCacheDir, httpCacheSize);
		    }catch(Exception e){
		    	Log.e("HttpResponseCache Issue", "Failed to set up com.integralblue.httpresponsecache.HttpResponseCache");
		    }
		}
	}
}
