package com.geeklabs.footmark.domain;

import java.io.Serializable;


public class FirstTrack implements Serializable {

	private Long id;
	private double lastTrackedLat;
	private double lastTrackedLng;
	private double lastKnownLat;
	private double lastKnownLng;
	private Long userId;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public double getLastTrackedLat() {
		return lastTrackedLat;
	}
	public void setLastTrackedLat(double lastTrackedLat) {
		this.lastTrackedLat = lastTrackedLat;
	}
	public double getLastTrackedLng() {
		return lastTrackedLng;
	}
	public void setLastTrackedLng(double lastTrackedLng) {
		this.lastTrackedLng = lastTrackedLng;
	}
	public double getLastKnownLat() {
		return lastKnownLat;
	}
	public void setLastKnownLat(double lastKnownLat) {
		this.lastKnownLat = lastKnownLat;
	}
	public double getLastKnownLng() {
		return lastKnownLng;
	}
	public void setLastKnownLng(double lastKnownLng) {
		this.lastKnownLng = lastKnownLng;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
}
