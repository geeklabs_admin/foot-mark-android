package com.geeklabs.footmark.util;

public class TimeAndDateUtil {
	public static int getHourIn12Format(int hour24) {
		int hourIn12Format = 0;

		if (hour24 == 0)
			hourIn12Format = 12;
		else if (hour24 <= 12)
			hourIn12Format = hour24;
		else
			hourIn12Format = hour24 - 12;

		return hourIn12Format;
	}

	public static String getAMPM(int time) {
		/*
		 * String ampm = (calendar.get(Calendar.AM_PM) == (Calendar.AM)) ? "AM"
		 * : "PM";
		 */
		String dayStatus = null;
		if (time == 0)
			dayStatus = "am";
		else if (time < 11)
			dayStatus = "am";
		else
			dayStatus = "pm";
		return dayStatus;

	}

}
