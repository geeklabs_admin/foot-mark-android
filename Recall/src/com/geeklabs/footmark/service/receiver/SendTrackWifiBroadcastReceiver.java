package com.geeklabs.footmark.service.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.geeklabs.footmark.service.SendTrackService;
import com.geeklabs.footmark.util.TrackingServiceManager;

public class SendTrackWifiBroadcastReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		
		if (TrackingServiceManager.canContinue(context)) {
			int wifiState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, WifiManager.WIFI_STATE_UNKNOWN);
			switch (wifiState) {
			case WifiManager.WIFI_STATE_ENABLED:
				Log.i("FM: System event type", intent.getAction());
				// Call send trackings to server service
				Intent sendTrackService = new Intent(context, SendTrackService.class);
				context.startService(sendTrackService);
				break;
			default:
				break;
			}

			 /*Intent i = new Intent(context, SendTrackService.class);
			 AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			 
			 PendingIntent pIntent = PendingIntent.getService(context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
			 
			 Calendar cal = Calendar.getInstance();
	         cal.add(Calendar.SECOND, 10); // 10 sec
	         
	         // Repeat for every 180 seconds
			 alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 60*1000, pIntent);*/
		}
	}
}
