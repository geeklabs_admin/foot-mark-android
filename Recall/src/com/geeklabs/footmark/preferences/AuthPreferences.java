package com.geeklabs.footmark.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class AuthPreferences {

	private static final String TRACKING_STATUS = "Running";
	private static final String USER_UID = "user_uid";
	private static final String USER_ID = "0";
	private static final String USER_ACCOUNT = "None";
	private static final String SIGN_IN_STATUS = "false";

	private static final String ACCESS_TOKEN = "access_token";
	private static final String EXPIRATION_TIME = "token_expiration_perion";
	private static final String REFRESH_TOKEN = "refresh_token";
	private static final String SCOPE_STRING = "scope";

	private static final String IS_REGISTERED = "registered";
	
	private SharedPreferences preferences;

	public AuthPreferences(Context context) {
		preferences = context.getSharedPreferences("footmark_user_pref", Context.MODE_PRIVATE);
	}

	public void setTokenExpirationTime(long time) {
		Editor editor = preferences.edit();
		editor.putLong(EXPIRATION_TIME, time);
		editor.commit();
	}

	public long getTokenExpirationTime() {
		return preferences.getLong(EXPIRATION_TIME, 0);
	}

	public void setScope(String scope) {
		Editor editor = preferences.edit();
		editor.putString(SCOPE_STRING, scope);
		editor.commit();
	}

	public String getScope() {
		return preferences.getString(SCOPE_STRING, null);
	}

	public void setAccessToken(String accessToken) {
		Editor editor = preferences.edit();
		editor.putString(ACCESS_TOKEN, accessToken);
		editor.commit();
	}

	public String getAccessToken() {
		return preferences.getString(ACCESS_TOKEN, null);
	}

	public void setRefreshToken(String refreshToken) {
		Editor editor = preferences.edit();
		editor.putString(REFRESH_TOKEN, refreshToken);
		editor.commit();
	}

	public String getRefreshToken() {
		return preferences.getString(REFRESH_TOKEN, null);
	}

	public void setTrackingStatus(String trackingStatus) {
		Editor editor = preferences.edit();
		editor.putString(TRACKING_STATUS, trackingStatus);
		editor.commit();
	}

	public String getTrackingStatus() {
		return preferences.getString(TRACKING_STATUS, "U/N");
	}

	public void setSignInStatus(boolean signInStatus) {
		Editor editor = preferences.edit();
		editor.putBoolean(SIGN_IN_STATUS, signInStatus);
		editor.commit();
	}

	public boolean isUserSignedIn() {
		return preferences.getBoolean(SIGN_IN_STATUS, false);
	}

	public void setUserUID(String user_uid) {
		Editor editor = preferences.edit();
		editor.putString(USER_UID, user_uid);
		editor.commit();
	}

	public String getUserUid() {
		return preferences.getString(USER_UID, null);
	}

	public void setUserAccount(String userAccount) {
		Editor editor = preferences.edit();
		editor.putString(USER_ACCOUNT, userAccount);
		editor.commit();
	}

	public String getUserAccount() {
		return preferences.getString(USER_ACCOUNT, null);
	}

	public void setUserId(long uId) {
		Editor editor = preferences.edit();
		editor.putLong(USER_ID, uId);
		editor.commit();
	}

	public long getUserId() {
		return preferences.getLong(USER_ID, 0);
	}

	public void setIsRegistered(boolean isRegistered) {
		Editor editor = preferences.edit();
		editor.putBoolean(IS_REGISTERED, isRegistered);
		editor.commit();
	}

	public boolean isRegistered() {
		return preferences.getBoolean(IS_REGISTERED, false);
	}

	public void clearCredentials() {
		Editor editor = preferences.edit();
		editor.remove(ACCESS_TOKEN);
		editor.remove(EXPIRATION_TIME);
		editor.remove(REFRESH_TOKEN);
		editor.remove(SCOPE_STRING);
		editor.commit();
	}
}