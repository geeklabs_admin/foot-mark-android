package com.geeklabs.footmark.domain;

public class GeneralSearch {
	
	private String query;
	
	public String getQuery() {
		return query;
	}
	
	public void setQuery(String query) {
		this.query = query;
	}
}
