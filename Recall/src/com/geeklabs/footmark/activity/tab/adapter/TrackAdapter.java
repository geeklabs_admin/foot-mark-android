package com.geeklabs.footmark.activity.tab.adapter;

import static com.geeklabs.footmark.activity.ActivityResultCode.CAPTURE_COMPLETE;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.geeklabs.footmark.R;
import com.geeklabs.footmark.activity.CaptureActivity;
import com.geeklabs.footmark.domain.CTrack;
import com.geeklabs.footmark.domain.OfflineTrack;
import com.geeklabs.footmark.domain.SyncedTrack;
import com.geeklabs.footmark.sqllite.SQLLiteManager;

public class TrackAdapter extends BaseAdapter {
	private Activity context;
	private List<CTrack> tracks = new ArrayList<CTrack>();
	private LayoutInflater mInflater;
	boolean status;

	public void setTracks(List<CTrack> tracks) {
		if (this.tracks.size() > 0) {
			this.tracks.addAll(tracks);
		} else {
			this.tracks = tracks;
		}
	}

	public List<CTrack> getTracks() {
		return tracks;
	}

	public TrackAdapter(Activity context) {
		this.context = context;
		mInflater = LayoutInflater.from(context);
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		final CTrack track = tracks.get(position);

		// TODO - Improve performance - reuse convertView and only change
		// content.
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.custom_row_view, null);
			holder = new ViewHolder();

			// holder.shareImage = (ImageView)
			// convertView.findViewById(R.id.share_image_view);

			holder.deleteImage = (ImageView) convertView.findViewById(R.id.delete_buttonon_image);

			// Track type image
			holder.navigationImage = (ImageView) convertView.findViewById(R.id.navigation_marker_image_view);
			holder.userImage = (ImageView) convertView.findViewById(R.id.user_icon);

			// Is track favorite images
			holder.favoriteImage = (ImageView) convertView.findViewById(R.id.favourite_image_view);
			holder.favoriteFocusedImage = (ImageView) convertView.findViewById(R.id.favourite_focused);

			holder.dateAndTime = (TextView) convertView.findViewById(R.id.date_time_text_view);
			holder.address = (TextView) convertView.findViewById(R.id.address_text_view);
			holder.content = (TextView) convertView.findViewById(R.id.content_text_view);
			holder.tag = (TextView) convertView.findViewById(R.id.tagView);

			// Set favorite icon
			if (track.isFavorite()) {
				holder.favoriteImage.setVisibility(View.INVISIBLE);
				holder.favoriteFocusedImage.setVisibility(View.VISIBLE);
			} else {
				holder.favoriteImage.setVisibility(View.VISIBLE);
				holder.favoriteFocusedImage.setVisibility(View.INVISIBLE);
			}

			convertView.setTag(holder);

			// on click action on content view
			holder.content.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					context.runOnUiThread(new Runnable() {
						public void run() {
							Intent intent = new Intent(context, CaptureActivity.class);
							intent.putExtra("selectedTrack", track);
							context.startActivityForResult(intent, CAPTURE_COMPLETE);
						}
					});
				}
			});

			// on click action on address view
			holder.address.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					context.runOnUiThread(new Runnable() {
						public void run() {
							Intent intent = new Intent(context, CaptureActivity.class);
							intent.putExtra("selectedTrack", track);
							context.startActivity(intent);
						}
					});

				}
			});

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		// Set date and time
		Date trackTime = track.getTrackTime();
		Locale currentLocale = context.getResources().getConfiguration().locale;
		String datePattern = "EEE, d MMM yyyy HH:mm (z)";
		SimpleDateFormat dateFormat = new SimpleDateFormat(datePattern, currentLocale);

		if (dateFormat != null && trackTime != null) {
			holder.dateAndTime.setText(dateFormat.format(trackTime));
		}

		// Set Address
		String address = track.getAddress();
		if (address != null && !address.isEmpty()) {
			holder.address.setText(Html.fromHtml(context.getResources().getString(R.string.you_are_at)) + address);
		} else if (track.getLatitude() > 0 && track.getLongitude() > 0) {
			holder.address.setText(Html.fromHtml(context.getResources().getString(R.string.you_are_at)) + "" + track.getLatitude() + "," + track.getLongitude());
		} else {
			holder.address.setText("Unknown Location");
		}

		// Set content
		String content = track.getContent();
		byte[] capturedImage = track.getCapturedImage();
		if (content != null && !content.isEmpty()) {
			holder.content.setVisibility(View.VISIBLE);
			holder.content.setText(content);
			holder.userImage.setVisibility(View.VISIBLE);
			holder.navigationImage.setVisibility(View.GONE);
		} else {
			holder.content.setText("");
			holder.navigationImage.setVisibility(View.VISIBLE);
			holder.userImage.setVisibility(View.GONE);
		}

		if (capturedImage != null && capturedImage.length > 0) {
			Bitmap bitmap = BitmapFactory.decodeByteArray(capturedImage, 0, capturedImage.length);
			holder.userImage.setImageBitmap(bitmap);
			holder.userImage.setVisibility(View.VISIBLE);
			holder.navigationImage.setVisibility(View.GONE);
		}

		// set tag
		String tag = track.getTag();
		if (tag != null && !tag.isEmpty()) {
			holder.tag.setText(tag);
		} else {
			holder.tag.setText("Default");
		}

		holder.deleteImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setTitle("Delete moment");
				builder.setMessage("Are you sure want to delete");
				builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
					}
				});

				builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (track instanceof OfflineTrack) {
							SQLLiteManager.getInstance(context).deleteSelectedOfflineTrack((OfflineTrack) track);
						} else {
							SQLLiteManager.getInstance(context).deleteSelectedSyncedTrack((SyncedTrack) track);
						}
					}
				});
				builder.show();
			}
		});
		holder.favoriteImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				track.setFavorite(true);
				if (track instanceof OfflineTrack) {
					SQLLiteManager.getInstance(context).updateOfflineTrackAsFavorite((OfflineTrack) track);
				} else {
					SQLLiteManager.getInstance(context).updateSyncedTrackAsFavorite((SyncedTrack) track);
				}
				
				holder.favoriteFocusedImage.setVisibility(View.VISIBLE);
				holder.favoriteImage.setVisibility(View.INVISIBLE);
			}
		});

		holder.favoriteFocusedImage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				track.setFavorite(false);
				if (track instanceof OfflineTrack) {
					SQLLiteManager.getInstance(context).updateOfflineTrackAsFavorite((OfflineTrack) track);
				} else {
					SQLLiteManager.getInstance(context).updateSyncedTrackAsFavorite((SyncedTrack) track);
				}
				
				holder.favoriteFocusedImage.setVisibility(View.INVISIBLE);
				holder.favoriteImage.setVisibility(View.VISIBLE);
			}
		});

		holder.navigationImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast toast = Toast.makeText(context, "clicked on Navigation Image ", Toast.LENGTH_SHORT);
				toast.show();

			}
		});

		return convertView;
	}

	@Override
	public int getCount() {
		return tracks.size();
	}

	@Override
	public Object getItem(int position) {
		return tracks.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	static class ViewHolder {
		ImageView deleteImage;
		ImageView shareImage;
		ImageView favoriteImage;
		ImageView navigationImage;
		ImageView favoriteFocusedImage;
		ImageView userImage;
		TextView dateAndTime;
		TextView address;
		TextView content;
		TextView tag;
	}
}
