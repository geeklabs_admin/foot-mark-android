package com.geeklabs.footmark.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class UserPreferences {

	private static final String DISTANCE = "distance";
	private static final String TIME = "time";
	private static final String ID = "id";

	private SharedPreferences preferences;

	public UserPreferences(Context context) {
		preferences = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
	}

	public void setDistance(float distance) {
		Editor editor = preferences.edit();
		editor.putFloat(DISTANCE, distance);
		editor.commit();
	}

	public float getDistance() {
		return preferences.getFloat(DISTANCE, 1f);
	}
	
	public void setId(long id) {
		Editor editor = preferences.edit();
		editor.putLong(ID, id);
		editor.commit();
	}

	public long getId() {
		return preferences.getLong(ID, 0);
	}

	public void setTime(int time) {
		Editor editor = preferences.edit();
		editor.putInt(TIME, time);
		editor.commit();
	}

	public int getTime() {
		return preferences.getInt(TIME, 3);
	}

	public void clearValues() {
		// Sets default values
		setTime(3);
		setDistance(1.0f);
		setId(0);
	}
}