package com.geeklabs.footmark.activity.tab.task;

import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.geeklabs.footmark.domain.OfflineTrack;
import com.geeklabs.footmark.sqllite.SQLLiteManager;

public abstract class GetOfflineTracksBaseTask extends AsyncTask<Void, ProgressDialog, List<OfflineTrack>> {

	private Context context;
	private int currentPage;

	public GetOfflineTracksBaseTask(Context context, int currentPage) {
		this.context = context;
		this.currentPage = currentPage;
	}

	@Override
	protected void onPreExecute() {
	}

	@Override
	protected void onPostExecute(List<OfflineTrack> tracks) {
		if (tracks != null && !tracks.isEmpty()) {
			updateUI(tracks);
		} else {
			stopProgressBar();
		}
	}

	@Override
	protected List<OfflineTrack> doInBackground(Void... params) {
		List<OfflineTrack> paginationOfflineTracks = SQLLiteManager.getInstance(context).getPaginationOfflineTracks(currentPage);
		return paginationOfflineTracks;
	}
	
	protected abstract void updateUI(List<OfflineTrack> tracks);

	protected abstract void stopProgressBar();
}
