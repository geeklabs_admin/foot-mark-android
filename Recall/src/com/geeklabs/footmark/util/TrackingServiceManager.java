package com.geeklabs.footmark.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.geeklabs.footmark.preferences.AuthPreferences;

public final class TrackingServiceManager {

	private static final String TRACK_STATUS_RUNNING = "Running"; 
	private static final String TRACK_STATUS_PAUSED = "Paused"; 
	
	private TrackingServiceManager(){}
	
	public static void setTrackingStatusToRunnung(Context contextActivity) {
		AuthPreferences authPreferences = new AuthPreferences(contextActivity);
		authPreferences.setTrackingStatus(TRACK_STATUS_RUNNING);
	}
	
	public static void setTrackingStatusToPaused(Context contextActivity) {
		AuthPreferences authPreferences = new AuthPreferences(contextActivity);
		authPreferences.setTrackingStatus(TRACK_STATUS_PAUSED);
	}
	
	public static boolean isUserLoggedIn(Context contextActivity) {
		AuthPreferences authPreferences = new AuthPreferences(contextActivity);
		
		return authPreferences.isUserSignedIn();
	}
	
	public static boolean isRunning(Context contextActivity) {
		AuthPreferences authPreferences = new AuthPreferences(contextActivity);
		String trackingState = authPreferences.getTrackingStatus();
		
		return TRACK_STATUS_RUNNING.equals(trackingState);
	}
	/**
	 * Checks whether track service is eligible for tracking.
	 */
	public static boolean canContinue(Context contextActivity) {
		AuthPreferences authPreferences = new AuthPreferences(contextActivity);
		
		boolean userSignedIn = authPreferences.isUserSignedIn();
		String trackingState = authPreferences.getTrackingStatus();
		
		return userSignedIn && TRACK_STATUS_RUNNING.equals(trackingState); // 
	}
	
	public static void startTracking(Activity contextActivity) {
		// Start tracking by starting service
		AuthPreferences authPreferences = new AuthPreferences(contextActivity);
		startTrackingService(contextActivity, authPreferences);
	}
	
	public static void stopTracking(Context contextActivity) {
		// Stop tracking
		AuthPreferences authPreferences = new AuthPreferences(contextActivity);
		stopTrackingService(contextActivity, authPreferences);
	}
	
	private static void stopTrackingService(Context contextActivity, AuthPreferences authPreferences) {
		// Stop scheduler
		Intent intent = new Intent();
	    intent.setAction(BroadcastEvent.STOP_TRACK_SERVICE);
	    contextActivity.sendBroadcast(intent);
	}

	private static void startTrackingService(Activity contextActivity, AuthPreferences authPreferences) {
		// start service and scheduler 
		Intent intent = new Intent();
	    intent.setAction(BroadcastEvent.START_TRACK_SERVICE);
	    contextActivity.sendBroadcast(intent);
	}
	
}
