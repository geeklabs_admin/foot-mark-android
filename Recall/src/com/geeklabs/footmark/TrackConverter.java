package com.geeklabs.footmark;

import com.geeklabs.footmark.domain.OfflineTrack;
import com.geeklabs.footmark.domain.SyncedTrack;

public final class TrackConverter {

	private TrackConverter() {}
	
	public static OfflineTrack convertToOfflineTrack(SyncedTrack syncedTrack) {
		OfflineTrack offlineTrack = new OfflineTrack();
		
		offlineTrack.setAddress(syncedTrack.getAddress());
		offlineTrack.setCapturedImage(syncedTrack.getCapturedImage());
		offlineTrack.setCapturedImageName(syncedTrack.getCapturedImageName());
		offlineTrack.setCapturedImagePath(syncedTrack.getCapturedImagePath());
		offlineTrack.setContent(syncedTrack.getContent());
		offlineTrack.setFavorite(syncedTrack.isFavorite());
		offlineTrack.setId(syncedTrack.getId());
		offlineTrack.setLatitude(syncedTrack.getLatitude());
		offlineTrack.setLongitude(syncedTrack.getLongitude());
		offlineTrack.setServerTrackId(syncedTrack.getServerTrackId());
		offlineTrack.setTag(syncedTrack.getTag());
		offlineTrack.setTrackTime(syncedTrack.getTrackTime());
		
		return offlineTrack;
	}
	
	public static SyncedTrack convertToSyncedTrack(OfflineTrack offlineTrack) {
		SyncedTrack syncedTrack = new SyncedTrack();
		
		syncedTrack.setAddress(offlineTrack.getAddress());
		syncedTrack.setCapturedImage(offlineTrack.getCapturedImage());
		syncedTrack.setCapturedImageName(offlineTrack.getCapturedImageName());
		syncedTrack.setCapturedImagePath(offlineTrack.getCapturedImagePath());
		syncedTrack.setContent(offlineTrack.getContent());
		syncedTrack.setFavorite(offlineTrack.isFavorite());
		syncedTrack.setId(offlineTrack.getId());
		syncedTrack.setLatitude(offlineTrack.getLatitude());
		syncedTrack.setLongitude(offlineTrack.getLongitude());
		syncedTrack.setServerTrackId(offlineTrack.getServerTrackId());
		syncedTrack.setTag(offlineTrack.getTag());
		syncedTrack.setTrackTime(offlineTrack.getTrackTime());
		
		return syncedTrack;
	}
}
