package com.geeklabs.footmark.activity;

import static com.geeklabs.footmark.activity.ActivityResultCode.CAPTURE_COMPLETE;

import java.util.ArrayList;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnActionExpandListener;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TabWidget;
import android.widget.Toast;

import com.geeklabs.footmark.R;
import com.geeklabs.footmark.activity.tab.OfflineTrackListFragmentTab;
import com.geeklabs.footmark.activity.tab.SyncedTrackListFragmentTab;
import com.geeklabs.footmark.communication.get.task.SignoutTask;
import com.geeklabs.footmark.preferences.AuthPreferences;
import com.geeklabs.footmark.util.HttpResponseUtil;
import com.geeklabs.footmark.util.TrackingServiceManager;
import com.google.android.gms.common.GooglePlayServicesUtil;

public class HomeFragment extends FragmentActivity {

	private TabHost mTabHost;
	private ViewPager mViewPager;
	private TabsAdapter mTabsAdapter;
	private Menu menu1;
	private MenuItem item2;
	private SearchView searchView;
	private AuthPreferences authPreferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

		setContentView(R.layout.home_tab_pager);
		// setSupportProgressBarIndeterminateVisibility(true);
		authPreferences = new AuthPreferences(this);
		// Installing an HTTP response cache
		HttpResponseUtil.enableHttpResponseCache(getCacheDir());

		// Action bar back ground color
		BitmapDrawable bitmap = (BitmapDrawable) getResources().getDrawable(
				R.drawable.steel_blue);
		bitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
		getActionBar().setBackgroundDrawable(bitmap);

		mTabHost = (TabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup();
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mTabsAdapter = new TabsAdapter(this, mTabHost, mViewPager);

		// Capture list view
		TabSpec captureTab = mTabHost.newTabSpec("CaptureList").setIndicator(
				"", getResources().getDrawable(R.drawable.ic_menu_mylocation));
		mTabsAdapter.addTab(captureTab, SyncedTrackListFragmentTab.class, null);

		// OfflineTab
		TabSpec OfflineTab = mTabHost.newTabSpec("Offline").setIndicator("",
				getResources().getDrawable(R.drawable.offline_tab));
		mTabsAdapter.addTab(OfflineTab, OfflineTrackListFragmentTab.class, null);

		// Map tab
		/*
		 * TabSpec mapTab = mTabHost.newTabSpec("Map").setIndicator("",
		 * getResources().getDrawable(R.drawable.map));
		 * mTabsAdapter.addTab(mapTab, MapFragmentTab.class, null);
		 */

		if (savedInstanceState != null) {
			mTabHost.setCurrentTabByTag(savedInstanceState.getString("tab"));
		}
	}

	@Override
	protected void onStop() {
		super.onStop();

		HttpResponseUtil.stopHttpResponseCache();
	}

	@Override
	protected void onResume() {
		super.onResume();
		LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		authPreferences = new AuthPreferences(this);
		if (!authPreferences.isUserSignedIn()) {
			Intent i = new Intent(this, FootMarkActivity.class);
			this.startActivity(i);
			
		} else if (authPreferences.isUserSignedIn() && GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext()) != 0 || !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)/* || locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)*/) {
			Toast.makeText(getApplicationContext(), "enable user network location service", Toast.LENGTH_LONG).show();
			
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString("tab", mTabHost.getCurrentTabTag());
	}

	public static class TabsAdapter extends FragmentPagerAdapter implements
			TabHost.OnTabChangeListener, ViewPager.OnPageChangeListener {

		private final Context mContext;
		private final TabHost mTabHost;
		private final ViewPager mViewPager;
		private final ArrayList<TabInfo> mTabs = new ArrayList<TabInfo>();

		static final class TabInfo {
			private final Class<?> clss;
			private final Bundle args;

			TabInfo(String _tag, Class<?> _class, Bundle _args) {
				clss = _class;
				args = _args;
			}
		}

		static class DummyTabFactory implements TabHost.TabContentFactory {
			private final Context mContext;

			public DummyTabFactory(Context context) {
				mContext = context;
			}

			@Override
			public View createTabContent(String tag) {

				View v = new View(mContext);
				v.setMinimumWidth(0);
				v.setMinimumHeight(0);
				return v;
			}
		}

		public TabsAdapter(FragmentActivity activity, TabHost tabHost,
				ViewPager pager) {
			super(activity.getSupportFragmentManager());
			mContext = activity;
			mTabHost = tabHost;
			mViewPager = pager;
			mTabHost.setOnTabChangedListener(this);

			mViewPager.setAdapter(this);
			mViewPager.setOnPageChangeListener(this);
		}

		public void addTab(TabHost.TabSpec tabSpec, Class<?> clss, Bundle args) {
			tabSpec.setContent(new DummyTabFactory(mContext));
			String tag = tabSpec.getTag();

			TabInfo info = new TabInfo(tag, clss, args);
			mTabs.add(info);
			mTabHost.addTab(tabSpec);

			notifyDataSetChanged();
		}

		@Override
		public void onPageScrollStateChanged(int arg0) {

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {

		}

		@Override
		public void onPageSelected(int position) {

			TabWidget widget = mTabHost.getTabWidget();
			int oldFocusability = widget.getDescendantFocusability();
			widget.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
			mTabHost.setCurrentTab(position);

			widget.setDescendantFocusability(oldFocusability);
		}

		@Override
		public void onTabChanged(String tabId) {
			int position = mTabHost.getCurrentTab();
			mViewPager.setBackgroundColor(Color.WHITE);

			// setTabColor(mTabHost);
			mViewPager.setCurrentItem(position);
		}

		@Override
		public Fragment getItem(int position) {
			TabInfo info = mTabs.get(position);

			return Fragment.instantiate(mContext, info.clss.getName(),
					info.args);
		}

		@Override
		public int getCount() {
			return mTabs.size();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		// Used to put dark icons on light action bar
				searchView = new SearchView(getApplicationContext());

				SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
				SearchableInfo searchableInfo = searchManager
						.getSearchableInfo(getComponentName());
				searchView.setSearchableInfo(searchableInfo);
				searchView.setQueryHint("Search moments�");

				searchView.setOnQueryTextListener(new OnQueryTextListener() {
					@Override
					public boolean onQueryTextSubmit(String query) {
						// TODO 
						return true;
					}

					@Override
					public boolean onQueryTextChange(String newText) {
						return false;
					}
				});

				menu.add("Search")
						.setIcon(android.R.drawable.ic_menu_search)
						.setActionView(searchView)
						.setShowAsAction(
								MenuItem.SHOW_AS_ACTION_ALWAYS
										| MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);

				menu.add("Advanced Search").setIcon(R.drawable.searchplus)
						.setVisible(false)
						.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

				if (TrackingServiceManager.isRunning(getApplicationContext())) {
					menu.add("Pause").setIcon(R.drawable.pause)
							.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
				} else {
					menu.add("Resume").setIcon(R.drawable.resume)
							.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
				}
				menu.add("Track_Me").setIcon(R.drawable.list1)
						.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
				menu.add("Sign Out").setTitle("Sign out").setIcon(R.drawable.sign_out)
						.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
				menu.add("Settings").setTitle("Settings").setIcon(R.drawable.settings)
						.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
				menu.add("Show tracks").setTitle("Show tracks")
						.setIcon(R.drawable.slideout_button)
						.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
				menu1 = menu;

				return true;
	}
	
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		AlertDialog.Builder builder = new AlertDialog.Builder(HomeFragment.this);

		if (item.getTitle().equals("Pause")) {

			builder.setTitle(R.string.alert);
			builder.setMessage(R.string.alert_pause_msg);
			builder.setPositiveButton("Yes",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							TrackingServiceManager
									.stopTracking(HomeFragment.this);
							item.setTitle("Resume").setIcon(R.drawable.resume);
						}
					});
			builder.setNegativeButton("No",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {

						}
					});
			builder.show();

			return true;
		} else if (item.getTitle().equals("Search")) {

			item2 = menu1.getItem(0);
			item2.setOnActionExpandListener(new OnActionExpandListener() {

				@Override
				public boolean onMenuItemActionExpand(MenuItem item) {
					item2 = menu1.getItem(2).setVisible(false);
					item2 = menu1.getItem(1).setVisible(false);
					item2 = menu1.getItem(3).setVisible(false);
					return true;
				}

				@Override
				public boolean onMenuItemActionCollapse(MenuItem item) {
					item2 = menu1.getItem(1).setVisible(false);
					item2 = menu1.getItem(2).setVisible(true);
					item2 = menu1.getItem(3).setVisible(true);
					return true;
				}
			});
			return true;

		} else if (item.getTitle().equals("Resume")) {

			TrackingServiceManager.startTracking(this);
			item.setTitle("Pause").setIcon(R.drawable.pause);
			return true;
		} else if (item.getTitle().equals("Sign out")) {
			// Register new user
			signoutUser();
			return true;
		} else if (item.getTitle().equals("Settings")) {
			Intent intent = new Intent(this, SettingsActivity.class);
			startActivity(intent);
		}  /*
		 * else if (item.getTitle().equals("Advanced Search")) { Intent intent =
		 * new Intent(this, AdvancedSearchActivity.class);
		 * startActivity(intent); }
		 */else if (item.getTitle().equals("Track_Me")) {
			Intent intent = new Intent(this, CaptureActivity.class);
			startActivityForResult(intent, CAPTURE_COMPLETE);
			return true;
		}
		// To go back to home activity by click on app icon
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void signoutUser() {
		final ProgressDialog signoutAPKProgressDialog = new ProgressDialog(
				HomeFragment.this);
		signoutAPKProgressDialog
				.setMessage("User signout is in progress, please wait...");
		Log.i("User signout", "User signout is in progress");
		signoutAPKProgressDialog.setCancelable(false);

		// Job required for validation, registration will be taken care in
		SignoutTask signoutTask = new SignoutTask(HomeFragment.this,
				signoutAPKProgressDialog);
		signoutTask.execute();
	}

	@Override
	public void onBackPressed() {
		moveTaskToBack(true);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
		case ActivityResultCode.CAPTURE_COMPLETE:
			mTabsAdapter.notifyDataSetChanged();
			break;

		default:
			break;
		}
	}
}
