package com.geeklabs.footmark.domain;

import java.io.Serializable;
import java.util.Date;

public class CTrack implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**
	 *  Id is the Android primary key column 
	 */
	protected int id;
	/**
	 * It is server Track primary key
	 */
	protected Long serverTrackId;
	private double latitude;
	private double longitude;
	protected Date trackTime;
	private String address;
	private String content;
	private boolean isFavorite;
	private String tag;
	
	private byte[] capturedImage;
	private String capturedImagePath;
	private String capturedImageName;
	
	private boolean isDeleted;

	public boolean isDeleted() {
		return isDeleted;
	}
	
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public double getLatitude() {
		return latitude;
	}
	
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	public double getLongitude() {
		return longitude;
	}
	
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	public Date getTrackTime() {
		return trackTime;
	}
	
	public void setTrackTime(Date trackTime) {
		this.trackTime = trackTime;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}

	public Long getServerTrackId() {
		return serverTrackId;
	}
	
	public void setServerTrackId(Long serverTrackId) {
		this.serverTrackId = serverTrackId;
	}
	
	public boolean isFavorite() {
		return isFavorite;
	}
	
	public void setFavorite(boolean isFavorite) {
		this.isFavorite = isFavorite;
	}
	
	public void setTag(String tag) {
		this.tag = tag;
	}
	
	public String getTag() {
		return tag;
	}
	
	

	public byte[] getCapturedImage() {
		return capturedImage;
	}
	
	public void setCapturedImage(byte[] capturedImage) {
		this.capturedImage = capturedImage;
	}
	
	public String getCapturedImagePath() {
		return capturedImagePath;
	}
	
	public void setCapturedImagePath(String capturedImagePath) {
		this.capturedImagePath = capturedImagePath;
	}
	
	public String getCapturedImageName() {
		return capturedImageName;
	}
	
	public void setCapturedImageName(String capturedImageName) {
		this.capturedImageName = capturedImageName;
	}
}
