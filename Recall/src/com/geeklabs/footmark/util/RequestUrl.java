package com.geeklabs.footmark.util;

public final class RequestUrl {

	private RequestUrl() {}
	
	/*public static final String VALIDATE_TOKEN_REQ = "http://192.168.1.107:8990/user/signin";
	public static final String USER_PREFERENCES = "http://192.168.1.107:8990/userpreferences/save/{id}";
	public static final String SIGNOUT_REQ = "http://192.168.1.107:8990/user/signout/{id}";
	public static final String SEND_TRACKS = "http://192.168.1.107:8990/track/saveTracks/{id}";
	public static final String GET_TRACKS_BY_DATE = "http://192.168.1.107:8990/track/byDate/{id}";
	public static final String GET_USER_TRACKS = "http://192.168.1.107:8990/track/userTracks/{id}";
	public static final String GET_DEFAULT_TRACKS = "http://192.168.1.107:8990/track/defaultTracks/{id}";
	public static final String DELETE_TRACK = "http://192.168.1.107:8990/track/delete/{id}";
	public static final String FAVOURITE_TRACKS = "http://192.168.1.107:8990/track/favouriteTracks";
	public static final String GENERIC_SEARCH = "http://192.168.1.107:8990/track/search/{id}";
	public static final String GET_FAVORITE_TRACKS = "http://192.168.1.107:8990/track/getFavorite/{id}";
	public static final String SEND_FIRST_TRACK = "http://192.168.1.107:8990/firstTrack/save/{id}";*/
	
	/*public static final String DELETE_TRACK = "http://geeklabsfm.appspot.com/track/delete/{id}";
	public static final String FAVOURITE_TRACKS = "http://geeklabsfm.appspot.com/track/favouriteTracks";
	public static final String VALIDATE_TOKEN_REQ = "http://geeklabsfm.appspot.com/user/signin";
	public static final String USER_PREFERENCES = "http://geeklabsfm.appspot.com/userpreferences/save/{id}";
	public static final String SIGNOUT_REQ = "http://geeklabsfm.appspot.com/user/signout/{id}";
	public static final String SEND_TRACKS = "http://geeklabsfm.appspot.com/track/saveTracks/{id}";
	public static final String GET_TRACKS = "http://geeklabsfm.appspot.com/track/defaultTracks/{id}";
	public static final String GET_TRACKS_BY_DATE = "http://geeklabsfm.appspot.com/track/byDate/{id}";
	public static final String GET_USER_TRACKS = "http://geeklabsfm.appspot.com/track/userTracks/{id}";
	public static final String GET_DEFAULT_TRACKS = "http://geeklabsfm.appspot.com/track/defaultTracks/{id}";
	public static final String GENERIC_SEARCH = "http://geeklabsfm.appspot.com/track/search/{id}";
	public static final String GET_FAVORITE_TRACKS = "http://geeklabsfm.appspot.com/track/getFavorite/{id}";*/

	/**
	 * Alpha release Urls, Enable below and create final APK.
	 */
	
	public static final String VALIDATE_TOKEN_REQ = "http://alpha.geeklabsfm.appspot.com/user/signin";
	public static final String USER_PREFERENCES = "http://alpha.geeklabsfm.appspot.com/userpreferences/save/{id}";
	public static final String SIGNOUT_REQ = "http://alpha.geeklabsfm.appspot.com/user/signout/{id}";
	public static final String SEND_TRACKS = "http://alpha.geeklabsfm.appspot.com/track/saveTracks/{id}";
	public static final String GET_TRACKS_BY_DATE = "http://alpha.geeklabsfm.appspot.com/track/byDate/{id}";
	public static final String GET_USER_TRACKS = "http://alpha.geeklabsfm.appspot.com/track/userTracks/{id}";
	public static final String GET_DEFAULT_TRACKS = "http://alpha.geeklabsfm.appspot.com/track/defaultTracks/{id}";
	public static final String DELETE_TRACK = "http://alpha.geeklabsfm.appspot.com/track/delete/{id}";
	public static final String FAVOURITE_TRACKS = "http://alpha.geeklabsfm.appspot.com/track/favouriteTracks";
	public static final String GENERIC_SEARCH = "http://alpha.geeklabsfm.appspot.com/track/search/{id}";
	public static final String GET_FAVORITE_TRACKS = "http://alpha.geeklabsfm.appspot.com/track/getFavorite/{id}";
	public static final String SEND_FIRST_TRACK = "http://alpha.geeklabsfm.appspot.com/firstTrack/save/{id}";
	
	
	/*public static final String VALIDATE_TOKEN_REQ = "http://alpha.geeksmstest.appspot.com/user/signin";
	public static final String USER_PREFERENCES = "http://geeksmstest.appspot.com/userpreferences/save/{id}";
	public static final String SIGNOUT_REQ = "http://geeksmstest.appspot.com/user/signout/{id}";
	public static final String SEND_TRACKS = "http://geeksmstest.appspot.com/track/saveTracks/{id}";
	public static final String GET_TRACKS_BY_DATE = "http://geeksmstest.appspot.com/track/byDate/{id}";
	public static final String GET_USER_TRACKS = "http://geeksmstest.appspot.com/track/userTracks/{id}";
	public static final String GET_DEFAULT_TRACKS = "http://geeksmstest.appspot.com/track/defaultTracks/{id}";
	public static final String DELETE_TRACK = "http://geeksmstest.appspot.com/track/delete/{id}";
	public static final String FAVOURITE_TRACKS = "http://geeksmstest.appspot.com/track/favouriteTracks";
	public static final String GENERIC_SEARCH = "http://geeksmstest.appspot.com/track/search/{id}";
	public static final String GET_FAVORITE_TRACKS = "http://geeksmstest.appspot.com/track/getFavorite/{id}";
	public static final String SEND_FIRST_TRACK = "http://geeksmstest.appspot.com/firstTrack/save/{id}";*/
	
}
