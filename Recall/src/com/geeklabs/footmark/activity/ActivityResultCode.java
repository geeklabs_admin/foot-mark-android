package com.geeklabs.footmark.activity;

public final class ActivityResultCode {

	private ActivityResultCode() {}
	
	public final static int SIGNIN_RESULT = 3;
	public final static int SIGNOUT_RESULT = 4;
	
	// For oauth
	public static final int ACCOUNT_CODE = 1601;
	
	// Capture to Home activity
	public static final int CAPTURE_COMPLETE = 5;

}
