package com.geeklabs.footmark.activity.tab.task;

import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.geeklabs.footmark.domain.SyncedTrack;
import com.geeklabs.footmark.sqllite.SQLLiteManager;

public abstract class GetSyncedTracksBaseTask extends AsyncTask<Void, ProgressDialog, List<SyncedTrack>> {

	private Context context;
	private int currentPage;

	public GetSyncedTracksBaseTask(Context context, int currentPage) {
		this.context = context;
		this.currentPage = currentPage;
	}

	@Override
	protected void onPreExecute() {
	}

	@Override
	protected void onPostExecute(List<SyncedTrack> tracks) {
		if (tracks != null && !tracks.isEmpty()) {
			updateUI(tracks);
		} else {
			stopProgressBar();
		}
	}

	@Override
	protected List<SyncedTrack> doInBackground(Void... params) {
		List<SyncedTrack> paginationSyncedTracks = SQLLiteManager.getInstance(context).getPaginationSyncedTracks(currentPage);
		return paginationSyncedTracks;
	}
	
	protected abstract void updateUI(List<SyncedTrack> tracks);

	protected abstract void stopProgressBar();
}
