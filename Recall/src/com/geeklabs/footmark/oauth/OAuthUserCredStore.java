package com.geeklabs.footmark.oauth;

import com.geeklabs.footmark.preferences.AuthPreferences;


/**
 * This class keeps the authentication tokens and the ID + SECRET
 * of the Google application.
 */

public final class OAuthUserCredStore {
	
	public static final String CLIENT_ID = "695372301993.apps.googleusercontent.com";
	public static final String CLIENT_SECRET = "98wzw9r8IGKyqUnuexEG-cPc";
	
	public static final String SCOPE = "https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile";
	public static final String REDIRECT_URI = "http://localhost";

	private final AuthPreferences prefs;

	private static OAuthUserCredStore store;

	private OAuthUserCredStore(AuthPreferences prefs) {
		this.prefs = prefs;
	}

	public static OAuthUserCredStore getInstance(AuthPreferences prefs) {
		if (store == null)
			store = new OAuthUserCredStore(prefs);

		return store;
	}

	public void clearCredentials() {
		prefs.clearCredentials();
	}
}
