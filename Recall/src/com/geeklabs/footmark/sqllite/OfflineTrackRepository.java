package com.geeklabs.footmark.sqllite;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.geeklabs.footmark.domain.OfflineTrack;

public class OfflineTrackRepository extends AbstractTrackRepository<OfflineTrack> {

	@Override
	public void save(OfflineTrack offlineTrack, SQLiteDatabase db) {
		StringBuilder sql = new StringBuilder("INSERT INTO OfflineTrack(lat, lng, date, content, tag," + " isFavorite, isDeleted, serverTrackId, capturedImage) "
				+ "values(?,?,?,?,?,?,?,?,?)");
		
		//		+ offlineTrack.getLatitude() + "," + " " + offlineTrack.getLongitude() + "," + " " + offlineTrack.getTrackTime().getTime() + ",");

		SQLiteStatement insertStmt      =   db.compileStatement(sql.toString());
		insertStmt.clearBindings();
		 
		insertStmt.bindDouble(1, offlineTrack.getLatitude());
		insertStmt.bindDouble(2, offlineTrack.getLongitude());
		insertStmt.bindLong(3, offlineTrack.getTrackTime().getTime());
		
		
		String content = offlineTrack.getContent();
		if (content != null && !content.isEmpty()) {
			insertStmt.bindString(4, content);
		} else {
			insertStmt.bindString(4, "");
		}

		String tag = offlineTrack.getTag();
		if (tag != null && !tag.isEmpty()) {
			insertStmt.bindString(5, tag);
		} else {
			insertStmt.bindString(5, "Default");
		}

		int isFav = offlineTrack.isFavorite() ? 1 : 0;
		int isDel = offlineTrack.isDeleted() ? 1 : 0;

		insertStmt.bindLong(6, isFav);
		insertStmt.bindLong(7, isDel);
		if (offlineTrack.getServerTrackId() != null) {
			insertStmt.bindLong(8, offlineTrack.getServerTrackId());
		} else {
			insertStmt.bindLong(8, 0l);
		}
		if (offlineTrack.getCapturedImage() != null && offlineTrack.getCapturedImage().length > 0) {
			insertStmt.bindBlob(9, offlineTrack.getCapturedImage());
		} else {
			insertStmt.bindBlob(9, new byte[]{});
		}
		
		insertStmt.executeInsert();
		
		Log.i("Offline Track info", "Offline Track info successfully saved -> " + "lat: " + offlineTrack.getLatitude() + "Lng:" + offlineTrack.getLongitude() + "content:"
				+ offlineTrack.getContent());
	}

	@Override
	public List<OfflineTrack> getTrackings(SQLiteDatabase db) {
		Cursor cursor = db.query(SQLLite.OFFLINE_TRACK_TABLE_NAME, null, null, null, null, null, null);
		return extractTracksFromCursor(cursor);
	}

	@Override
	public void deleteAllTrackings(SQLiteDatabase db) {
		db.execSQL("DELETE FROM " + SQLLite.OFFLINE_TRACK_TABLE_NAME + " ");
		Log.i("Delete All offline Tracking info", "Successfully deleted existing all offline trackings from database");
	}

	public boolean isServerTrackExist(Long trackId, SQLiteDatabase db) {
		Cursor cursor = db.query(SQLLite.OFFLINE_TRACK_TABLE_NAME, null, " serverTrackId = ?", new String[] { String.valueOf(trackId) }, null, null, null);
		boolean isServerTrackIdExist = cursor.moveToFirst();
		cursor.close();
		return isServerTrackIdExist;
	}

	public void updateOfflineTrackAsFavorite(OfflineTrack track, SQLiteDatabase db) {
		int isFav = track.isFavorite() ? 1 : 0;
		db.execSQL("UPDATE " + SQLLite.OFFLINE_TRACK_TABLE_NAME + " set isFavorite= '" + isFav + "' where id= '" + track.getId() + "'");
		Log.i("Update Offline favorite", "favorite is successfully updated in Offline track");
	}

	public void deleteSelectedOfflineTrack(OfflineTrack track, SQLiteDatabase db) {
		db.execSQL("DELETE FROM " + SQLLite.OFFLINE_TRACK_TABLE_NAME + " where id= '" + track.getId() + "'");
		Log.i("delete Offline", "deleted successfully from Offline track");
	}

	// FIXME - Image
	public void updateOfflineTrack(OfflineTrack track, SQLiteDatabase db) {
		db.execSQL("UPDATE " + SQLLite.OFFLINE_TRACK_TABLE_NAME + " set content= '" + track.getContent() + "',tag= '" + track.getTag() + "' where id= '"
				+ track.getId() + "'");
		Log.i("updateTrack", "track updated successfully");
	}

	public List<OfflineTrack> getPaginationOfflineTracks(int currentPage, SQLiteDatabase db) {
		int offset = 0;
		if (currentPage > 0) {
			offset = SQLLite.limit * currentPage;
		}

		Cursor cursor = db.query(SQLLite.OFFLINE_TRACK_TABLE_NAME, null, " serverTrackId is null or serverTrackId=?", new String[] {"0"}, null, null, "date", " " + offset + ", " + SQLLite.limit);
		return extractTracksFromCursor(cursor);
	}

	private List<OfflineTrack> extractTracksFromCursor(Cursor cursor) {
		List<OfflineTrack> trackings = new ArrayList<OfflineTrack>();
		cursor.moveToFirst();
		while (cursor.isAfterLast() == false) {
			OfflineTrack track = new OfflineTrack();

			if (cursor.getInt(0) > 0) {
				track.setId(cursor.getInt(0));
			}
			track.setLatitude(cursor.getDouble(1));
			track.setLongitude(cursor.getDouble(2));
			track.setTrackTime(new Date(cursor.getLong(3)));
			track.setContent(cursor.getString(4));
			track.setTag(cursor.getString(5));
			track.setFavorite(cursor.getInt(6) > 0);
			track.setDeleted(cursor.getInt(7) > 0);

			long serverTrackId = cursor.getLong(8);

			Long serverTrack = Long.valueOf(serverTrackId);
			if (serverTrack > 0 && serverTrack != null) {
				track.setServerTrackId(serverTrack);
			}

			track.setCapturedImage(cursor.getBlob(9));
			
			trackings.add(track);
			cursor.moveToNext();
		}
		cursor.close();
		Log.i("Get Tracking info", "Successfully got existing trackings");
		return trackings;
	}

}
