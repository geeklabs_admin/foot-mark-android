package com.geeklabs.footmark.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class TrackAlgPreferences {

	private static final String LAST_TRACKED_LAT = "0.0";
	private static final String LAST_TRACKED_LNG = "0.1";
	private static final String LAST_KNOWN_LAT = "0.2";
	private static final String LAST_KNOWN_LNG = "0.3";
	
	private SharedPreferences preferences;
	
	public TrackAlgPreferences(Context context) {
		preferences = context.getSharedPreferences("FM_Track_Shared_Pref", Context.MODE_PRIVATE);
	}
	
	public void setLastTrackedLat(String lastTrackedLat) {
		Editor editor = preferences.edit();
		editor.putString(LAST_TRACKED_LAT, lastTrackedLat);
		editor.commit();
	}
	
	public String getLastTrackedLat() {
		return preferences.getString(LAST_TRACKED_LAT, LAST_TRACKED_LAT);
	}
	
	public void setLastTrackedLng(String lastTrackedLng) {
		Editor editor = preferences.edit();
		editor.putString(LAST_TRACKED_LNG, lastTrackedLng);
		editor.commit();
	}
	
	public String getLastTrackedLng() {
		return preferences.getString(LAST_TRACKED_LNG, LAST_TRACKED_LNG);
	}
	
	public void setLastKnownLat(String lastKnownLat) {
		Editor editor = preferences.edit();
		editor.putString(LAST_KNOWN_LAT, lastKnownLat);
		editor.commit();
	}
	
	public String getLastKnownLat() {
		return preferences.getString(LAST_KNOWN_LAT, LAST_KNOWN_LAT);
	}
	
	public void setLastKnownLng(String lastKnownLng) {
		Editor editor = preferences.edit();
		editor.putString(LAST_KNOWN_LNG, lastKnownLng);
		editor.commit();
	}
	
	public String getLastKnownLng() {
		return preferences.getString(LAST_KNOWN_LNG, LAST_KNOWN_LNG);
	}
}

