package com.geeklabs.footmark.communication;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;

import android.app.ProgressDialog;
import android.content.Context;

public abstract class AbstractHttpPostTask extends BaseTask {

	public AbstractHttpPostTask(ProgressDialog progressDialog, Context context) {
		super(progressDialog, context);
	}

	@Override
	protected void doRequestSpecificTask(HttpURLConnection conn) throws IOException {
		conn.setRequestMethod(HttpRequestType.POST.name());
		conn.setDoOutput(true);//set it to true if you want to send (output) a request body, for example with POST or PUT requests. 
		
		OutputStream out = new BufferedOutputStream(conn.getOutputStream());

		// Get json object to send in request
		final String jsonRequestContent = getRequestJSON();
		
		out.write(jsonRequestContent.getBytes());// write request BODY content
		out.close();
	}
	
	protected abstract String getRequestJSON();
}
