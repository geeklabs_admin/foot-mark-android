package com.geeklabs.footmark.service.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.geeklabs.footmark.service.SendTrackService;
import com.geeklabs.footmark.util.TrackingServiceManager;

public class SendTrackBroadcastReceiver extends BroadcastReceiver {

	 @Override
	   public void onReceive(Context context, Intent intent) {
		 if (TrackingServiceManager.canContinue(context)) {
			 Log.i("FM: System event type", intent.getAction());
			 // Call send trackings to server service
			 Intent sendTrackService = new Intent(context, SendTrackService.class);
			 context.startService(sendTrackService);
		 }
	 }
}
