package com.geeklabs.footmark.response.json;

public class ResponseStatus {
	
	private String status;
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}

}
