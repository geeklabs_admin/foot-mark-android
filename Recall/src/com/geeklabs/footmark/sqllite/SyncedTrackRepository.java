package com.geeklabs.footmark.sqllite;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.geeklabs.footmark.domain.SyncedTrack;

public class SyncedTrackRepository extends AbstractTrackRepository<SyncedTrack> {

	@Override
	public void save(SyncedTrack syncedTrack, SQLiteDatabase db) {
		
		StringBuilder sql = new StringBuilder("INSERT INTO SyncedTrack(lat, lng, date, content, tag,isFavorite, serverTrackId,address, capturedImage) "
				+ "values(?,?,?,?,?,?,?,?,?)");
		
		//		+ offlineTrack.getLatitude() + "," + " " + offlineTrack.getLongitude() + "," + " " + offlineTrack.getTrackTime().getTime() + ",");

		SQLiteStatement insertStmt      =   db.compileStatement(sql.toString());
		insertStmt.clearBindings();
		 
		insertStmt.bindDouble(1, syncedTrack.getLatitude());
		insertStmt.bindDouble(2, syncedTrack.getLongitude());
		insertStmt.bindLong(3, syncedTrack.getTrackTime().getTime());
		
		String content = syncedTrack.getContent();
		if (content != null && !content.isEmpty()) {
			insertStmt.bindString(4, content);
		} else {
			insertStmt.bindString(4, "");
		}

		String tag = syncedTrack.getTag();
		if (tag != null && !tag.isEmpty()) {
			insertStmt.bindString(5, tag);
		} else {
			insertStmt.bindString(5, "Default");
		}

		int isFav = syncedTrack.isFavorite() ? 1 : 0;

		insertStmt.bindLong(6, isFav);
		if (syncedTrack.getServerTrackId() != null) {
			insertStmt.bindLong(7, syncedTrack.getServerTrackId());
		} else {
			insertStmt.bindLong(7, 0l);
		}
		
		String address = syncedTrack.getAddress();
		if (address != null && !address.isEmpty()) {
			insertStmt.bindString(8, address );
		} else {
			insertStmt.bindString(8, "Unknown");
		}
		
		if (syncedTrack.getCapturedImage() != null && syncedTrack.getCapturedImage().length > 0) {
			insertStmt.bindBlob(9, syncedTrack.getCapturedImage());
		} else {
			insertStmt.bindBlob(9, new byte[]{});
		}
		
		insertStmt.executeInsert();
		
		Log.i("Synced Track info", "Synced Track info successfully saved -> " + "lat: " + syncedTrack.getLatitude() + "Lng:" + syncedTrack.getLongitude() + "content:" + syncedTrack.getContent());
	}

	@Override
	public List<SyncedTrack> getTrackings(SQLiteDatabase db) {
		Cursor cursor = db.query(SQLLite.SYNCED_TRACK_TABLE_NAME, null, null, null, null, null, null);
		
		return extractTracksFromCursor(cursor);
	}

	@Override
	public void deleteAllTrackings(SQLiteDatabase db) {
		db.execSQL("DELETE FROM " + SQLLite.SYNCED_TRACK_TABLE_NAME + " ");
		Log.i("Delete All synced Tracking info", "Successfully deleted existing all synced trackings from database");
	}

	public void updateTrackAsFavorite(SyncedTrack track, SQLiteDatabase db) {
		int isFav = track.isFavorite() ? 1 : 0;
		db.execSQL("UPDATE " + SQLLite.SYNCED_TRACK_TABLE_NAME + " set isFavorite= '" + isFav + "' where serverTrackId= '" + track.getServerTrackId() + "'");
		Log.i("Update favorite", "favorite is successfully updated in Synced track");
	}
	
	public void deleteSelectedTrack(SyncedTrack track, SQLiteDatabase db) {
		db.execSQL("DELETE FROM " + SQLLite.SYNCED_TRACK_TABLE_NAME + " where serverTrackId= '" + track.getServerTrackId() + "'");
		Log.i("delete", "deleted successfully from Synced track");
	}
	
	// FIXME - Image
	public void updateSyncedTrack(SyncedTrack track, SQLiteDatabase db) {
		db.execSQL("UPDATE " + SQLLite.SYNCED_TRACK_TABLE_NAME + " set content= '" + track.getContent() + "',tag= '" + track.getTag() + "' where serverTrackId= '"
					+ track.getServerTrackId() + "'");
		Log.i("updateTrack", "track updated successfully");
	}

	/**
	 * http://stackoverflow.com/questions/14996574/pagination-in-listview
	 * http://stackoverflow.com/questions/3325515/sqlite-limit-offset-query
	 */
	public List<SyncedTrack> getPaginationSyncedTracks(int currentPage, SQLiteDatabase db) {
		int offset = 0;
		if (currentPage > 0) {
			offset = SQLLite.limit * currentPage;
		}
		
		// LIMIT <skip>, <count> Is equivalent to LIMIT <count> OFFSET <skip> -> 100,50 -> 100 is offset and 50 is the limit
		// 
		Cursor cursor = db.query(SQLLite.SYNCED_TRACK_TABLE_NAME, null, null, null, null, null, "date", " "+offset+", "+SQLLite.limit);
		return extractTracksFromCursor(cursor);
	}
	
	private List<SyncedTrack> extractTracksFromCursor(Cursor cursor) {
		List<SyncedTrack> trackings = new ArrayList<SyncedTrack>();
		cursor.moveToFirst();
		while (cursor.isAfterLast() == false) {
			SyncedTrack track = new SyncedTrack();

			if (cursor.getInt(0) > 0) {
				track.setId(cursor.getInt(0));
			}
			track.setLatitude(cursor.getDouble(1));
			track.setLongitude(cursor.getDouble(2));
			track.setTrackTime(new Date(cursor.getLong(3)));
			track.setContent(cursor.getString(4));
			track.setTag(cursor.getString(5));
			track.setFavorite(cursor.getInt(6) > 0);

			long serverTrackId = cursor.getLong(7);

			track.setAddress(cursor.getString(8));
			
			Long serverTrack = Long.valueOf(serverTrackId);
			if (serverTrack > 0 && serverTrack != null) {
				track.setServerTrackId(serverTrack);
			}

			track.setCapturedImage(cursor.getBlob(9));
			
			trackings.add(track);
			cursor.moveToNext();
		}
		cursor.close();
		Log.i("Get synced Tracking info", "Successfully got existing synced trackings");
		return trackings;
	}
}
