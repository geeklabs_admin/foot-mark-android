package com.geeklabs.footmark.service;

import android.app.IntentService;
import android.content.Intent;

import com.geeklabs.footmark.communication.AbstractHttpPostTask;
import com.geeklabs.footmark.communication.post.task.SendTrackTask;
import com.geeklabs.footmark.util.NetworkService;

public class SendTrackService extends IntentService {

	public SendTrackService() {
		super("Send tracks to server");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// Send request to server if n/w available
		if (NetworkService.isNetWorkAvailable(getApplicationContext())) {
			AbstractHttpPostTask sendTrackRequest = new SendTrackTask(getApplicationContext(), null);
			sendTrackRequest.execute();
		}
	}
}
