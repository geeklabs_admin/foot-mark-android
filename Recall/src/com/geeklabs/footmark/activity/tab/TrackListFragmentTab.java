package com.geeklabs.footmark.activity.tab;

import java.util.List;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;

import com.geeklabs.footmark.activity.tab.adapter.TrackAdapter;
import com.geeklabs.footmark.communication.get.task.GetDefaultTracksTask;
import com.geeklabs.footmark.domain.SyncedTrack;
import com.geeklabs.footmark.event.EndlessScrollListener;
import com.geeklabs.footmark.util.NetworkService;

public class TrackListFragmentTab extends ListFragment {
	private TrackAdapter adapter;

	@Override
	public void onResume() {
		super.onResume();
		// Get tracks if not showing on UI
		if (adapter != null && adapter.getTracks().isEmpty() && NetworkService.isNetWorkAvailable(getActivity())) {
			getTracks(true);
			adapter.notifyDataSetChanged();
		} /*else if (adapter != null && NetworkService.isNetWorkAvailable(getActivity())) { // Get latest tracks everytime
			adapter.setTracks(new ArrayList<Track>());
			getTracks(false);
			adapter.notifyDataSetChanged();
		}*/
	}

//	http://stackoverflow.com/questions/12779041/android-fragments-content-view-not-yet-created?
	/*@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		 View view = inflater.inflate(R.layout.layout, null);
		   return view; //You must return your view here
	}*/
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);

		Log.i("Loading Tracks", "Loading your moments");

		adapter = new TrackAdapter(getActivity());

		setListAdapter(adapter);

		// Listen for scroll change, Are we at the end of list view ?
		EndlessScrollListener endlessScrollListener = new EndlessScrollListener(0) {
			@Override
			protected void runTask(int currentPage) {
				// get tracks in background async task
				GetDefaultTracksTask getDefaultTracksTask = new GetDefaultTracksTask(currentPage, TrackListFragmentTab.this.getActivity(), null) {
					@Override
					protected void updateUI(final List<SyncedTrack> tracks) {
						
						getActivity().runOnUiThread(new Runnable() {
							public void run() {
								getActivity().setProgressBarIndeterminateVisibility(false);
								// Notify adapter with new track list
								if (adapter != null && tracks != null && !tracks.isEmpty()) {
									//adapter.setTracks(tracks);
									adapter.notifyDataSetChanged();
								}
							}
						});
					}

					@Override
					protected void stopProgressBar() {
						getActivity().setProgressBarIndeterminateVisibility(false);
					}
				};

				getDefaultTracksTask.execute();
				getActivity().setProgressBarIndeterminateVisibility(true);
			}
		};
		getListView().setOnScrollListener(endlessScrollListener);
	}

	private void getTracks(final boolean showMsg) {

		// get tracks in background async task
		GetDefaultTracksTask getDefaultTracksTask = new GetDefaultTracksTask(0, this.getActivity(), null) {
			@Override
			protected void updateUI(final List<SyncedTrack> tracks) {
				getActivity().runOnUiThread(new Runnable() {
					public void run() {
						// Notify adapter with new track list
						if (adapter != null && tracks != null && !tracks.isEmpty()) {
							//adapter.setTracks(tracks);
							adapter.notifyDataSetChanged();
						}
						getActivity().setProgressBarIndeterminateVisibility(false);
					}
				});
			}

			@Override
			protected void stopProgressBar() {
				if (showMsg) {
					showEmptyText("No moments captured...");
				}
				getActivity().setProgressBarIndeterminateVisibility(false);
			}

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				if (isNetworkAvailable) {
					getActivity().setProgressBarIndeterminateVisibility(true);
				}
			}
		};
		getDefaultTracksTask.execute();
	}
	
	private void showEmptyText(final String msg) {
		View root = getView();
		if (root != null) {
			setEmptyText(msg);
		}
	}
}
