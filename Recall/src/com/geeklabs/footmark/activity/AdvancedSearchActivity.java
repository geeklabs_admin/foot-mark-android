/*package com.geeklabs.footmark.activity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.res.Configuration;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.geeklabs.footmark.R;
import com.geeklabs.footmark.communication.get.task.AdvancedSearchTask;
import com.geeklabs.footmark.constants.FootMarkConstants;
import com.geeklabs.footmark.domain.AdvanceSearch;
import com.geeklabs.footmark.util.TimeAndDateUtil;

public class AdvancedSearchActivity extends SherlockFragmentActivity implements
		TextWatcher {

	private Button fromTime_Button, toDate_Button, toTime_Button,
			fromDate_Button;
	private int toHour, toMinute, fromMinute, toYear, fromYear, toMonth,
			fromMonth, toDay, fromDay, fromHour, currentYear, currentMonth,
			currentDay, currentHour, currentMin, toDay1, toMonth1, toYear1,
			fromDay1, fromMonth1, fromYear1;
	private Calendar currentDate, selectedDate;
	private long diffInMillisec = 0;
	private int checkStatus = 0, checkStatus1 = 0;
	private String dayStatus;
	private AutoCompleteTextView autotext1, autotext2;

	private static final int TIME_DIALOG_FROM_ID = 1;
	private static final int DATE_DIALOG_FROM_ID = 2;
	private static final int TIME_DIALOG_TO_ID = 3;
	private static final int DATE_DIALOG_TO_ID = 4;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.advanced_search);

		// Set back ground color to action bar
		BitmapDrawable bitmap = (BitmapDrawable) getResources().getDrawable(
				R.drawable.steel_blue);
		bitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
		getSupportActionBar().setBackgroundDrawable(bitmap);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		addDateAndTimeListener();

		autotext1 = (AutoCompleteTextView) findViewById(R.id.countryNameEditText);
		autotext2 = (AutoCompleteTextView) findViewById(R.id.state_editText);

		autotext1.addTextChangedListener(this);
		autotext2.addTextChangedListener(this);

		// Auto drop down for countries
		autotext1.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,FootMarkConstants.COUNTRY_NAMES));
		autotext1.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				autotext1.showDropDown();
				return false;
			}
		});
		// Auto drop down for states
		autotext2.setAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_dropdown_item_1line,
				FootMarkConstants.STATE_NAMES));

		autotext2.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				autotext2.showDropDown();
				return false;
			}
		});

	}

	public void addDateAndTimeListener() {
		final Calendar calendar = Calendar.getInstance();
		
		toYear = calendar.get(Calendar.YEAR);
		toMonth = calendar.get(Calendar.MONTH);
		toDay = calendar.get(Calendar.DAY_OF_MONTH);

		fromYear = calendar.get(Calendar.YEAR);
		fromMonth = calendar.get(Calendar.MONTH);
		fromDay = calendar.get(Calendar.DAY_OF_MONTH);

		toHour = TimeAndDateUtil.getHourIn12Format(calendar.get(Calendar.HOUR_OF_DAY));
		toMinute = calendar.get(Calendar.MINUTE);

		fromHour = TimeAndDateUtil.getHourIn12Format(calendar.get(Calendar.HOUR_OF_DAY));
		fromMinute = calendar.get(Calendar.MINUTE);

		dayStatus = TimeAndDateUtil.getAMPM(Calendar.HOUR_OF_DAY);

		fromTime_Button = (Button) findViewById(R.id.fromTime);
		toTime_Button = (Button) findViewById(R.id.toTime);

		fromDate_Button = (Button) findViewById(R.id.fromDate);
		toDate_Button = (Button) findViewById(R.id.toDate);

		fromTime_Button.setText(new StringBuilder().append(padding_str(12))
				.append(":").append(padding_str(00)).append("am"));
		toTime_Button.setText(new StringBuilder().append(padding_str(toHour))
				.append(":").append(padding_str(toMinute)).append(dayStatus));

		fromDate_Button
				.setText(new StringBuilder().append(fromDay).append("-")
						.append(fromMonth + 1).append("-").append(fromYear)
						.append(" "));
		toDate_Button.setText(new StringBuilder().append(toDay).append("-")
				.append(toMonth + 1).append("-").append(toYear).append(" "));

		fromTime_Button.setOnClickListener(new OnClickListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {

				showDialog(TIME_DIALOG_FROM_ID);
			}
		});
		toTime_Button.setOnClickListener(new OnClickListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {

				showDialog(TIME_DIALOG_TO_ID);
			}
		});

		fromDate_Button.setOnClickListener(new OnClickListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {

				showDialog(DATE_DIALOG_FROM_ID);

			}
		});
		toDate_Button.setOnClickListener(new OnClickListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {

				showDialog(DATE_DIALOG_TO_ID);
			}
		});
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case TIME_DIALOG_FROM_ID:
			// set time picker as current time
			return new TimePickerDialog(this, from_timePickerListener,
					fromHour, fromMinute, false);
		case TIME_DIALOG_TO_ID:
			return new TimePickerDialog(this, to_timePickerListener, toHour,
					toMinute, false);
		case DATE_DIALOG_FROM_ID:
			return new DatePickerDialog(this, from_datePickerListener,
					fromYear, fromMonth, fromDay);
		case DATE_DIALOG_TO_ID:
			return new DatePickerDialog(this, to_datePickerListener, toYear,
					toMonth, toDay);
		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener from_datePickerListener = new DatePickerDialog.OnDateSetListener() {
		// when dialog box is closed, below method will be called.
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			final Calendar calendar = Calendar.getInstance();
			checkStatus = 2;
			currentYear = calendar.get(Calendar.YEAR);
			currentMonth = calendar.get(Calendar.MONTH);
			currentDay = calendar.get(Calendar.DAY_OF_MONTH);
			fromYear = selectedYear;
			fromMonth = selectedMonth;
			fromDay = selectedDay;
			currentDate = Calendar.getInstance();
			selectedDate = Calendar.getInstance();
			currentDate.set(currentYear, currentMonth, currentDay);
			selectedDate.set(fromYear, fromMonth, fromDay);

			diffInMillisec = currentDate.getTimeInMillis()
					- selectedDate.getTimeInMillis();
			if (diffInMillisec >= 0) {
				setSelectedFromDate(fromYear, fromMonth, fromDay);
				fromDate_Button.setText(new StringBuilder().append(fromDay)
						.append("-").append(fromMonth + 1).append("-")
						.append(fromYear).append(" "));
			} else {
				showToastForDate();
				checkStatus = 0;
			}
		}
	};
	private DatePickerDialog.OnDateSetListener to_datePickerListener = new DatePickerDialog.OnDateSetListener() {
		// when dialog box is closed, below method will be called.
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			final Calendar calendar = Calendar.getInstance();

			currentYear = calendar.get(Calendar.YEAR);
			currentMonth = calendar.get(Calendar.MONTH);
			currentDay = calendar.get(Calendar.DAY_OF_MONTH);
			checkStatus = 1;
			toYear = selectedYear;
			toMonth = selectedMonth;
			toDay = selectedDay;
			currentDate = Calendar.getInstance();
			selectedDate = Calendar.getInstance();
			currentDate.set(currentYear, currentMonth, currentDay);

			selectedDate.set(toYear, toMonth, toDay);
			diffInMillisec = currentDate.getTimeInMillis()
					- selectedDate.getTimeInMillis();
			if (diffInMillisec >= 0) {

				setSelectedToDate(toYear, toMonth, toDay);
				toDate_Button.setText(new StringBuilder().append(toDay)
						.append("-").append(toMonth + 1).append("-")
						.append(toYear).append(" "));

			} else {
				showToastForDate();
			}
		}

	};

	public int getFromYear() {
		return fromYear1;
	}

	public int getFromMonth() {
		return fromMonth1;
	}

	public int getFromDay() {
		return fromDay1;
	}

	public int getToYear() {
		return toYear1;
	}

	public int getToMonth() {
		return toMonth1;
	}

	public int getToDay() {
		return toDay1;
	}

	public void setSelectedFromDate(int year, int month, int day) {
		// final Calendar calendar = Calendar.getInstance();

		fromYear1 = year;
		fromMonth1 = month;
		fromDay1 = day;
	}

	public void setSelectedToDate(int year, int month, int day) {
		// final Calendar calendar = Calendar.getInstance();
		toYear1 = year;
		toMonth1 = month;
		toDay1 = day;
	}

	public void showToastForDate() {
		Toast.makeText(this, "Please Choose Correct date", Toast.LENGTH_SHORT)
				.show();
	}

	public void showToastForTime() {
		Toast.makeText(this, "Please Choose Correct time", Toast.LENGTH_SHORT)
				.show();
	}

	private TimePickerDialog.OnTimeSetListener from_timePickerListener = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int selectedHour,
				int selectedMinute) {
			final Calendar calendar = Calendar.getInstance();
			fromHour = TimeAndDateUtil.getHourIn12Format(selectedHour);
			fromMinute = selectedMinute;
			Calendar calendar1 = Calendar.getInstance();
			toHour = TimeAndDateUtil.getHourIn12Format(selectedHour);
			toMinute = selectedMinute;
			dayStatus = TimeAndDateUtil.getAMPM(selectedHour);
			currentYear = calendar.get(Calendar.YEAR);
			currentMonth = calendar.get(Calendar.MONTH);
			currentDay = calendar.get(Calendar.DAY_OF_MONTH);
			currentHour = calendar.get(Calendar.HOUR_OF_DAY);
			currentMin = calendar.get(Calendar.MINUTE);
			if (checkStatus1 == 0) {

				setSelectedFromDate(currentYear, currentMonth, currentDay);
				checkStatus1++;
			}
			calendar.set(Calendar.YEAR, currentYear);
			calendar.set(Calendar.MONTH, currentMonth);
			calendar.set(Calendar.DAY_OF_MONTH, currentDay);
			calendar.set(Calendar.HOUR_OF_DAY, currentHour);
			calendar.set(Calendar.MINUTE, currentMin);
			Date date1 = calendar.getTime();

			calendar1.set(Calendar.YEAR, getFromYear());
			calendar1.set(Calendar.MONTH, getFromMonth());
			calendar1.set(Calendar.DAY_OF_MONTH, getFromDay());
			calendar1.set(Calendar.HOUR_OF_DAY, selectedHour);
			calendar1.set(Calendar.MINUTE, selectedMinute - 1);
			Date date = calendar1.getTime();

			if (date.before(date1))
				fromTime_Button.setText(new StringBuilder()
						.append(padding_str(fromHour)).append(":")
						.append(padding_str(fromMinute)).append(dayStatus));
			else
				showToastForTime();
		}

	};

	private TimePickerDialog.OnTimeSetListener to_timePickerListener = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int selectedHour,
				int selectedMinute) {
			final Calendar calendar = Calendar.getInstance();
			Calendar calendar1 = Calendar.getInstance();
			toHour = TimeAndDateUtil.getHourIn12Format(selectedHour);
			toMinute = selectedMinute;
			dayStatus = TimeAndDateUtil.getAMPM(selectedHour);
			currentYear = calendar.get(Calendar.YEAR);
			currentMonth = calendar.get(Calendar.MONTH);
			currentDay = calendar.get(Calendar.DAY_OF_MONTH);
			currentHour = calendar.get(Calendar.HOUR_OF_DAY);
			currentMin = calendar.get(Calendar.MINUTE);
			calendar.set(Calendar.YEAR, currentYear);
			calendar.set(Calendar.MONTH, currentMonth);
			calendar.set(Calendar.DAY_OF_MONTH, currentDay);
			calendar.set(Calendar.HOUR_OF_DAY, currentHour);
			calendar.set(Calendar.MINUTE, currentMin);
			Date date1 = calendar.getTime();
			if (checkStatus == 0) {

				setSelectedToDate(currentYear, currentMonth, currentDay);
				checkStatus++;
			}

			calendar1.set(Calendar.YEAR, getToYear());
			calendar1.set(Calendar.MONTH, getToMonth());
			calendar1.set(Calendar.DAY_OF_MONTH, getToDay());
			calendar1.set(Calendar.HOUR_OF_DAY, selectedHour);
			calendar1.set(Calendar.MINUTE, selectedMinute - 1);
			Date date = calendar1.getTime();

			if (date.before(date1))
				toTime_Button.setText(new StringBuilder()
						.append(padding_str(toHour)).append(":")
						.append(padding_str(toMinute)).append(dayStatus));
			else {
				showToastForTime();

			}

		}
	};

	private static String padding_str(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}

	@Override
	public void afterTextChanged(Editable arg0) {

	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {

	}

	
	 * @Override public void onBackPressed() { Intent intent = new Intent(this,
	 * HomeFragment.class); NavUtils.navigateUpTo(this, intent); }
	 

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

		// Checks the orientation of the screen
		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
			Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
		} else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
			Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add("search").setIcon(R.drawable.search)
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		// To go back to home activity by click on app icon
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		}

		if (item.getTitle().equals("search")) {
			Toast.makeText(getApplicationContext(), "search button",
					Toast.LENGTH_LONG).show();
			AdvanceSearch advanceSearch = new AdvanceSearch();
			String from = fromDate_Button.getText().toString()
					.concat((String) fromTime_Button.getText().toString());
			from.replace("am", ":00").concat("am");
			
			String to = toDate_Button.getText().toString()
					.concat((String) toTime_Button.getText().toString());
			to.replace("pm", ":00").concat("pm");
			
			SimpleDateFormat dateFormat = new SimpleDateFormat(
					"yyyy-mm-dd HH:MM", Locale.getDefault());
			Date fromDate = null;
			Date toDate = null;
			
			try {
				toDate = dateFormat.parse(to);
				fromDate = dateFormat.parse(from);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			advanceSearch.setFromDate(fromDate);
			advanceSearch.setFromDate(toDate);

			ProgressDialog dialog = new ProgressDialog(
					AdvancedSearchActivity.this);
			dialog.setMessage("loading... plz wait");
			dialog.setCancelable(false);
			dialog.show();

			AdvancedSearchTask advancedSearchTask = new AdvancedSearchTask(
					AdvancedSearchActivity.this, dialog, advanceSearch);
			advancedSearchTask.execute();
		}

		return super.onOptionsItemSelected(item);
	}

}
*/