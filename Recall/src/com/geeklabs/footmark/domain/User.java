package com.geeklabs.footmark.domain;


public class User {
	
	private String accessToken;
	private String userUuid;
	private String status = "error";
	private long userId;
	private Preferences preferencesDto;
	private String resignIn = "false";
	private String device;
	private String model;
	private String product;
	private String user;
	private String sdk; 
	private int sdkInt; 
	
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getUserUuid() {
		return userUuid;
	}
	public void setUserUuid(String userUuid) {
		this.userUuid = userUuid;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public long getUserId() {
		return userId;
	}
	
	public Preferences getPreferencesDto() {
		return preferencesDto;
	}
	
	public void setPreferencesDto(Preferences preferencesDto) {
		this.preferencesDto = preferencesDto;
	}
	
	public String getResignIn() {
		return resignIn;
	}
	
	public void setResignIn(String resignIn) {
		this.resignIn = resignIn;
	}
	
	public String getDevice() {
		return device;
	}
	
	public void setDevice(String device) {
		this.device = device;
	}
	
	public String getModel() {
		return model;
	}
	
	public void setModel(String model) {
		this.model = model;
	}
	
	public String getProduct() {
		return product;
	}
	
	public void setProduct(String product) {
		this.product = product;
	}
	
	public String getUser() {
		return user;
	}
	
	public void setUser(String user) {
		this.user = user;
	}
	
	public String getSdk() {
		return sdk;
	}
	
	public void setSdk(String sdk) {
		this.sdk = sdk;
	}
	
	public int getSdkInt() {
		return sdkInt;
	}
	
	public void setSdkInt(int sdkInt) {
		this.sdkInt = sdkInt;
	}
}
