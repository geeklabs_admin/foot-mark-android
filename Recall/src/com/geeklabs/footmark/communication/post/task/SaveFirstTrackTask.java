package com.geeklabs.footmark.communication.post.task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.JSONException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.geeklabs.footmark.activity.HomeFragment;
import com.geeklabs.footmark.communication.AbstractHttpPostTask;
import com.geeklabs.footmark.domain.SyncedTrack;
import com.geeklabs.footmark.preferences.AuthPreferences;
import com.geeklabs.footmark.sqllite.SQLLiteManager;
import com.geeklabs.footmark.util.RequestUrl;

public class SaveFirstTrackTask extends AbstractHttpPostTask {
	private Activity context;
	private AuthPreferences authPreferences;
	private SyncedTrack track;

	public SaveFirstTrackTask(Activity context, ProgressDialog progressDialog, SyncedTrack track) {
		super(progressDialog, context);
		this.context = context;
		this.track = track;
		authPreferences = new AuthPreferences(context);
	}

	@Override
	protected void showMessageOnUI(final String message) {
		context.runOnUiThread(new Runnable() {
			public void run() {
				Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
				Log.i("FM save track", message);
			}
		});
	}

	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		/**
		 * Not required to take any action as user doesn't need to know first track status.
		 */
		
		if (!jsonResponse.isEmpty()) {
			// response is success, then remove tracks from db
			ObjectMapper mapper = new ObjectMapper();
			List<SyncedTrack> tracks = new ArrayList<SyncedTrack>();
			// get all trackings from response
			try {
				tracks = mapper.readValue(jsonResponse, new TypeReference<List<SyncedTrack>>() {
				});
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			//save Synced Tracks
			for (SyncedTrack syncedTrack : tracks) {
				SQLLiteManager.getInstance(context).saveSyncedTrack(syncedTrack);
			}

			Intent i = new Intent(context, HomeFragment.class);
			context.startActivity(i);
			context.finish();
		}	
	}

	@Override
	protected String getRequestJSON() {
		List<SyncedTrack> trackings = new ArrayList<SyncedTrack>();
		trackings.add(track);

		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(trackings);
		} catch (Exception e) {
			throw new IllegalStateException("problem occured json parsing while sending tracks");
		}
	}

	@Override
	protected String executeTaskInOffline() {

		/*boolean serverTrackExist = SQLLiteManager.getInstance(context).isServerTrackExist(track.getServerTrackId());
		if (serverTrackExist) {
			SQLLiteManager.getInstance(context).updateTrack(track);
		} else if (track.getId() > 0) {
			SQLLiteManager.getInstance(context).updateTrack(track);
		} else {
			SQLLiteManager.getInstance(context).saveTrack(track);
		}
		ResponseStatus responseStatus = new ResponseStatus();
		responseStatus.setStatus("success");
		ObjectMapper mapper = new ObjectMapper();
		String status;
		try {
			status = mapper.writeValueAsString(responseStatus);
			return status;
		} catch (Exception e) {
			e.printStackTrace();
		}*/

		return super.executeTaskInOffline();
	}

	@Override
	protected String getRequestUrl() {
		return RequestUrl.SEND_TRACKS.replace("{id}", String.valueOf(authPreferences.getUserId()));
	}
}
