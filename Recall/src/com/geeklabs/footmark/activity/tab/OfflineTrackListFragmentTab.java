package com.geeklabs.footmark.activity.tab;

import static com.geeklabs.footmark.activity.ActivityResultCode.CAPTURE_COMPLETE;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.geeklabs.footmark.activity.CaptureActivity;
import com.geeklabs.footmark.activity.tab.adapter.TrackAdapter;
import com.geeklabs.footmark.activity.tab.task.GetOfflineTracksBaseTask;
import com.geeklabs.footmark.domain.CTrack;
import com.geeklabs.footmark.domain.OfflineTrack;
import com.geeklabs.footmark.event.EndlessScrollListener;
import com.geeklabs.footmark.util.NetworkService;

public class OfflineTrackListFragmentTab extends ListFragment {
	private TrackAdapter adapter;

	@Override
	public void onResume() {
		super.onResume();
		// Get tracks if not showing on UI
		// execute if list is empty
		if (adapter != null && adapter.getTracks().isEmpty()) {
			getTracks(true);
			adapter.notifyDataSetChanged();
		} /*
		 * else if (adapter != null &&
		 * NetworkService.isNetWorkAvailable(getActivity())) { // Get latest
		 * tracks everytime // Empty list adapter.setTracks(new
		 * ArrayList<Track>()); adapter.notifyDataSetChanged();
		 * 
		 * // Get latest list getTracks(false); adapter.notifyDataSetChanged();
		 * }
		 */
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);

		Log.i("Loading Tracks", "Loading your moments");

		adapter = new TrackAdapter(getActivity());
		setListAdapter(adapter);

		// Listen for scroll change, Are we at the end of list view ?
		if (NetworkService.isNetWorkAvailable(getActivity())) {
			EndlessScrollListener endlessScrollListener = new EndlessScrollListener(0) {
				@Override
				protected void runTask(int currentPage) {
					// get tracks in background async task
					GetOfflineTracksBaseTask syncedTracksBaseTask = new GetOfflineTracksBaseTask(getActivity(), currentPage) {
						@Override
						protected void updateUI(final List<OfflineTrack> tracks) {
							getActivity().runOnUiThread(new Runnable() {
								public void run() {
									// Notify adapter with new track list
									if (adapter != null && tracks != null && !tracks.isEmpty()) {

										List<CTrack> cTracks = new ArrayList<CTrack>();
										cTracks.addAll(tracks);

										adapter.setTracks(cTracks);
										adapter.notifyDataSetChanged();
									}
								}
							});
						}

						@Override
						protected void stopProgressBar() {
							showEmptyText("Oops no offline moments captured yet ...");
							getActivity().setProgressBarIndeterminateVisibility(false);
						}

					};
					syncedTracksBaseTask.execute();
					getActivity().setProgressBarIndeterminateVisibility(true);
				}
			};
			getListView().setOnScrollListener(endlessScrollListener);
		}

		// On list item clicked
		getListView().setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				CTrack cTrack = adapter.getTracks().get(position);
				Intent intent = new Intent(getActivity(), CaptureActivity.class);
				intent.putExtra("selectedTrack", cTrack);
				getActivity().startActivityForResult(intent, CAPTURE_COMPLETE);

			}
		});
	}

	private void showEmptyText(final String msg) {
		View root = getView();
		if (root != null) {
			setEmptyText(msg);
		}
	}

	private void getTracks(final boolean showMsg) {
		// get tracks in background async task
		GetOfflineTracksBaseTask getCapturedTracks = new GetOfflineTracksBaseTask(this.getActivity(), 0) {
			@Override
			protected void updateUI(final List<OfflineTrack> tracks) {
				getActivity().runOnUiThread(new Runnable() {
					public void run() {
						// Notify adapter with new track list
						if (adapter != null && tracks != null && !tracks.isEmpty()) {
							List<CTrack> cTracks = new ArrayList<CTrack>();
							cTracks.addAll(tracks);

							adapter.setTracks(cTracks);
							adapter.notifyDataSetChanged();
						}
						getActivity().setProgressBarIndeterminateVisibility(false);
					}
				});
			}

			@Override
			protected void stopProgressBar() {
				if (showMsg) {
					showEmptyText("Oops no moments captured yet ...");
				}
				getActivity().setProgressBarIndeterminateVisibility(false);
			}

		};
		getCapturedTracks.execute();
		getActivity().setProgressBarIndeterminateVisibility(true);
	}
}