package com.geeklabs.footmark.communication.post.task;

import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.geeklabs.footmark.activity.FootMarkActivity;
import com.geeklabs.footmark.activity.HomeFragment;
import com.geeklabs.footmark.communication.AbstractHttpPostTask;
import com.geeklabs.footmark.domain.Preferences;
import com.geeklabs.footmark.domain.SyncedTrack;
import com.geeklabs.footmark.domain.User;
import com.geeklabs.footmark.preferences.AuthPreferences;
import com.geeklabs.footmark.preferences.TrackAlgPreferences;
import com.geeklabs.footmark.preferences.UserPreferences;
import com.geeklabs.footmark.util.RequestUrl;
import com.geeklabs.footmark.util.TrackingServiceManager;

public class RegisterUserTask extends AbstractHttpPostTask {
	private AuthPreferences authPreferences;
	private Activity contextActivity;
	private String userUid;
	private String accessToken;
	private UserPreferences userPreferences;
	private ProgressDialog signingAPKProgressDialog;
	private User user;
	private SyncedTrack firstTrack;
	private TrackAlgPreferences locationPreferences;

	public RegisterUserTask(Activity context, ProgressDialog signingAPKProgressDialog, String accessToken, User user2, SyncedTrack firstTrack) {
		super(signingAPKProgressDialog, context);
		this.contextActivity =  context;
		this.accessToken = accessToken;
		this.firstTrack = firstTrack;
		authPreferences = new AuthPreferences(context);
		userUid = UUID.randomUUID().toString();
		userPreferences = new UserPreferences(context);
		locationPreferences = new TrackAlgPreferences(context);
		user = user2;
	}
	
	@Override
	protected void showMessageOnUI(final String message) {
		contextActivity.runOnUiThread(new Runnable() {
			public void run() {
				Toast.makeText(contextActivity, message,
						Toast.LENGTH_SHORT).show();
				Log.i("FM Request Processing", message);
			}
		});
	}

	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			 user = mapper.readValue(jsonResponse.toString(), User.class);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String status = user.getStatus();
			
		if ("success".equals(status) || "register".equals(status)) {
			// Set user sign in status 
			authPreferences.setSignInStatus(true);
			authPreferences.setAccessToken(accessToken);
			
			//set register status
			authPreferences.setIsRegistered(true);
			
			// User id
			long userId = user.getUserId();
			authPreferences.setUserId(userId);
			
			//set user_uuid
			authPreferences.setUserUID(userUid);
			
			//update preferences
			Preferences preferences = user.getPreferencesDto();
			if (preferences != null) {
				userPreferences.setDistance(preferences.getDistance());
				userPreferences.setTime(preferences.getTime());
				userPreferences.setId(preferences.getId());
			}
			
			// Start tracking service
			TrackingServiceManager.startTracking(contextActivity);
			
			// send first track
			if ("register".equals(status)) {
				//set last tracked in preferences
				locationPreferences.setLastTrackedLat(String.valueOf(firstTrack.getLatitude()));
				locationPreferences.setLastTrackedLng(String.valueOf(firstTrack.getLongitude()));
				
				locationPreferences.setLastKnownLat(String.valueOf(firstTrack.getLatitude()));
				locationPreferences.setLastKnownLng(String.valueOf(firstTrack.getLongitude()));
				
				firstTrack.setContent("Thank you for choosing Footmark.");
				firstTrack.setTrackTime(new Date());
				
				SaveFirstTrackTask trackTask = new SaveFirstTrackTask(contextActivity, null, firstTrack);
				trackTask.execute();				
			} else {
				Intent i = new Intent(contextActivity, HomeFragment.class);
				contextActivity.startActivity(i);
				contextActivity.finish();
			}
			
			showMessageOnUI("Successfully logged in");
			cancelDialog();
		} else if("UuidInActive".equals(status)) {
			AlertDialog.Builder builder = new AlertDialog.Builder(contextActivity);
			builder.setTitle("User Login Issue");
			builder.setMessage("With this account, application is running in another device. Are you sure you want to singin again with this device");
			builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Intent i = new Intent(contextActivity, FootMarkActivity.class);
					contextActivity.startActivity(i);
					contextActivity.finish();
				}
			});
			
			builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					user.setResignIn("true");
					
					user.setDevice(android.os.Build.DEVICE); 
					user.setModel(android.os.Build.MODEL);
					user.setProduct(android.os.Build.PRODUCT);
					user.setUser(android.os.Build.USER);
					user.setSdkInt(android.os.Build.VERSION.SDK_INT);
					
					// Again send request to sign in
					RegisterUserTask registerUserTask = new RegisterUserTask(contextActivity, signingAPKProgressDialog, accessToken, user, null);
					registerUserTask.execute();
				}
			});
			
			builder.show();
		} else {
			showMessageOnUI("Unable to reach server, try after sometime");
			cancelDialog();
			Intent i = new Intent(contextActivity, FootMarkActivity.class);
			contextActivity.startActivity(i);
		}
	}

	@Override
	protected String getRequestJSON() {
		final JSONObject jsonObject = new JSONObject();
		
		try {
			jsonObject.put("accessToken", accessToken);
			jsonObject.put("userUuid", userUid);
			jsonObject.put("resignIn",user.getResignIn());
			jsonObject.put("device",user.getDevice());
			jsonObject.put("model",user.getModel());
			jsonObject.put("product",user.getProduct());
			//jsonObject.put("user",user.getUser());
			//jsonObject.put("sdk",user.getSdk());
			//jsonObject.put("sdkInt",user.getSdkInt());
			
		} catch (JSONException e) {
			e.printStackTrace();
			showMessageOnUI("Problem occured while authenticating user, try again");
			cancelDialog();
			Log.e("Authentication", "User authentication is failed" + e.getMessage());
		}
		return jsonObject.toString();
	}

	@Override
	protected String getRequestUrl() {
		return RequestUrl.VALIDATE_TOKEN_REQ;
	}
}