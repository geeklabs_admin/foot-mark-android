package com.geeklabs.footmark.activity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.geeklabs.footmark.Images;
import com.geeklabs.footmark.R;
import com.geeklabs.footmark.domain.CTrack;
import com.geeklabs.footmark.domain.OfflineTrack;
import com.geeklabs.footmark.domain.SyncedTrack;
import com.geeklabs.footmark.service.TrackingService;
import com.geeklabs.footmark.sqllite.SQLLiteManager;

public class CaptureActivity extends Activity {

	private EditText captureEditText, tagEditText;
	private TextView countDownTextView;
	private CTrack selectedTrack;
	private TrackingService trackingService;
	private ImageButton gallaryImageButton;
	private byte[] capturedImage;
	private Bitmap bitmap;
	private ImageView momentImage;
	private File textFile;
	private String capturedImageFileName;
	private View cameraImage;

	private static final int SELECT_IMAGE = 560;
	private static final int SELECT_IMAGE_CAMERA = 565;

	private ServiceConnection mConnection = new ServiceConnection() {

		public void onServiceConnected(ComponentName className, IBinder binder) {
			trackingService = ((TrackingService.UserLocationBinder) binder).getService();
		}

		public void onServiceDisconnected(ComponentName className) {
			trackingService = null;
		}
	};

	@Override
	protected void onResume() {
		super.onResume();
		bindService(new Intent(this, TrackingService.class), mConnection, Context.BIND_AUTO_CREATE);
	}

	@Override
	protected void onPause() {
		super.onPause();
		unbindService(mConnection);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.img_text_capture);

		// hide keypad
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		// Move cursor at end of text
		EditText et = (EditText) findViewById(R.id.captureText);
		et.setSelection(et.getText().length());
		// et.append(et.getText());

		// get Gallary Image Button
		gallaryImageButton = (ImageButton) findViewById(R.id.GallaryImageButton);

		// Camera
		cameraImage = (ImageButton) findViewById(R.id.camera_imageButton);

		// get image to fix
		momentImage = (ImageView) findViewById(R.id.moment_image);

		// get Share button
		ImageButton shareImageBtton = (ImageButton) findViewById(R.id.share_imageButton);
		// Hide tag edit text and text view
		findViewById(R.id.editTag).setVisibility(View.GONE);

		selectedTrack = (CTrack) getIntent().getSerializableExtra("selectedTrack");

		// Action bar back ground color
		BitmapDrawable bitmap = (BitmapDrawable) getResources().getDrawable(R.drawable.steel_blue);
		bitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
		getActionBar().setBackgroundDrawable(bitmap);

		// for back to home page
		getActionBar().setDisplayHomeAsUpEnabled(true);

		textWatcher();

		oncliKOnCameraButton();

		onclickOnShareButton(shareImageBtton);

		onClickOnGallaryImageButton(gallaryImageButton);
	}

	private void onClickOnGallaryImageButton(ImageButton gallaryImageButton) {
		gallaryImageButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				getImage();
			}
		});
	}

	private void oncliKOnCameraButton() { // open camera
		cameraImage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
				startActivityForResult(intent, SELECT_IMAGE_CAMERA);
			}
		});
	}

	protected void getImage() {
		Intent imagePicker = new Intent(Intent.ACTION_GET_CONTENT);
		imagePicker.setType("image/*");
		startActivityForResult(imagePicker, SELECT_IMAGE);
	}

	private void onclickOnShareButton(ImageButton shareImageBtton) {

	}

	private void showAlert() {

		if (captureEditText.getText().length() == 0 && tagEditText.getText().length() == 0) {
			onBackPressed();
			// finish();

		} else {

			AlertDialog.Builder builder = new AlertDialog.Builder(CaptureActivity.this);
			builder.setTitle("Capture Text");
			builder.setMessage("Discard changes?");
			builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {

				}
			});
			builder.setNeutralButton("Discard", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					onBackPressed();
				}
			});

			builder.show();
		}
	}

	private void textWatcher() {
		captureEditText = (EditText) findViewById(R.id.captureText);
		// to get cursor end of the text
		captureEditText.setSelection(captureEditText.getText().length());
		countDownTextView = (TextView) findViewById(R.id.countdownTextView);
		tagEditText = (EditText) findViewById(R.id.editTag);

		// Set text if exist i.e. Update mode.
		if (selectedTrack != null) {
			captureEditText.setText(selectedTrack.getContent());
			tagEditText.setText(selectedTrack.getTag());
			findViewById(R.id.editTag).setVisibility(View.VISIBLE);

			byte[] capturedImage2 = selectedTrack.getCapturedImage();
			if (capturedImage2 != null && selectedTrack.getCapturedImage().length > 0) {
				bitmap = BitmapFactory.decodeByteArray(capturedImage2, 0, capturedImage2.length);
				momentImage.setVisibility(View.VISIBLE);
				momentImage.setImageBitmap(bitmap);
			} else {
				momentImage.setVisibility(View.GONE);
			}
		} else { // New track or capture
			momentImage.setVisibility(View.GONE);
		}

		final TextWatcher mTextEditorWatcher = new TextWatcher() {
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// This sets a text view to the current length
				countDownTextView.setText(String.valueOf(150 - s.length()));
				findViewById(R.id.editTag).setVisibility(View.VISIBLE);
				if (s.length() == 0) {
					findViewById(R.id.editTag).setVisibility(View.GONE);
				}
			}

			public void afterTextChanged(Editable s) {
				if (s.length() > 0) {
					String str = captureEditText.getText().toString();
					char t = str.charAt(0);
					if (!Character.isLetter(t)) {
					}
				}
			}
		};
		captureEditText.addTextChangedListener(mTextEditorWatcher);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add("save").setIcon(R.drawable.save).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			if (captureEditText.getText().length() == 0) {
				onBackPressed();
			} else {
				showAlert();
			}
		}
		// If not enter text when click on save button
		if (item.getTitle().equals("save")) {
			String capturedText = captureEditText.getText().toString().trim();
			Context ctx = getApplicationContext();
			if (capturedText.length() == 0) {
				Toast toast = Toast.makeText(ctx, "Please enter text", Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
				toast.show();
			}

			// Remove the spaces from string if there are more than one space
			if (capturedText.trim().length() > 0) {
				String content = capturedText.trim();
				String tag = null;
				Editable tagText = tagEditText.getText();
				if (tagText != null) {
					tag = tagText.toString();
				}

				if (selectedTrack != null) { // Edit mode
					selectedTrack.setContent(content);
					selectedTrack.setTag(tag);
				} else { // Save mode
					selectedTrack = new OfflineTrack();
					selectedTrack.setContent(content);
					selectedTrack.setTag(tag);
					selectedTrack.setTrackTime(new Date());

					// FIXME-get current user location and update here.
					if (trackingService != null && trackingService.getLocation() != null) {
						Location location = trackingService.getLocation();
						selectedTrack.setLatitude(location.getLatitude());
						selectedTrack.setLongitude(location.getLongitude());
					}
				}

				// captured image into bytes
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				if (bitmap != null) {
					bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
					capturedImage = stream.toByteArray();
					selectedTrack.setCapturedImage(capturedImage);
					selectedTrack.setCapturedImageName(capturedImageFileName);
				}

				final ProgressDialog saveTrackProgressDialog = new ProgressDialog(CaptureActivity.this);
				saveTrackProgressDialog.setMessage("Saving your moment, please wait...");
				Log.i("Save track", "Savetrack is in progress");
				saveTrackProgressDialog.setCancelable(false);

				saveTrackProgressDialog.show();
				try {
					if (selectedTrack.getId() > 0 || (selectedTrack.getServerTrackId() != null && selectedTrack.getServerTrackId() > 0)) { // Edit mode
						if (selectedTrack instanceof SyncedTrack) {
							// Update synced track
							SQLLiteManager.getInstance(ctx).updateSyncedTrack((SyncedTrack) selectedTrack);
						} else {
							// Make new entry into offline table, so that we can
							// update server
							SQLLiteManager.getInstance(ctx).updateOfflineTrack((OfflineTrack) selectedTrack);
						}
					} else {
						// Save only into offline track table, update to sync
						// and server later
						SQLLiteManager.getInstance(ctx).saveOfflineTrack((OfflineTrack) selectedTrack);
					}
					Toast.makeText(ctx, "Saved your moment!", Toast.LENGTH_SHORT).show();
				} finally {
					saveTrackProgressDialog.cancel();
					Intent i = new Intent(ctx, HomeFragment.class);
					i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					
					startActivity(i);
					finish();
				}
			}
		}

		return super.onOptionsItemSelected(item);
	}

	// results for galleryImage
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if ((requestCode == SELECT_IMAGE || requestCode == SELECT_IMAGE_CAMERA) && resultCode == RESULT_OK) {
			Uri selectedFileUri = data.getData();
			String imagePath = getRealPath(selectedFileUri);

			/*
			 * BitmapFactory.Options options = new BitmapFactory.Options();
			 * options.inJustDecodeBounds = false; options.inPreferredConfig =
			 * Config.RGB_565; options.inDither = true;
			 */

			bitmap = Images.decodeSampledBitmapFromResource(imagePath, 100, 100);
			if (imagePath != null) {
				textFile = new File(imagePath);
				if (textFile.isFile()) {
					capturedImageFileName = textFile.getName();
				}
			}

			momentImage.setImageBitmap(bitmap);
			momentImage.setVisibility(View.VISIBLE);

		} else if (resultCode == RESULT_CANCELED) {
			//
			Intent intent = new Intent(this, CaptureActivity.class);
			startActivity(intent);
			finish();
		}
	}

	// to get the selected file/image path
	public String getRealPath(Uri selectedFileUri) {
		if (selectedFileUri == null) {
			return null;
		}
		Cursor cursor = getContentResolver().query(selectedFileUri, null, null, null, null);
		if (cursor == null) {
			return selectedFileUri.getPath();
		} else {
			cursor.moveToFirst();
			int idx = cursor.getColumnIndex(MediaStore.Files.FileColumns.DATA);
			String string = cursor.getString(idx);
			cursor.close();
			return string;
		}
	}
}
