package com.geeklabs.footmark.communication;

import java.net.HttpURLConnection;
import java.net.ProtocolException;

import android.app.ProgressDialog;
import android.content.Context;

public abstract class AbstractHttpGetTask extends BaseTask {

	public AbstractHttpGetTask(ProgressDialog progressDialog, Context context) {
		super(progressDialog, context);
	}
	
	@Override
	protected void doRequestSpecificTask(HttpURLConnection conn) throws ProtocolException {
		conn.setRequestMethod(HttpRequestType.GET.name());
	}
}
