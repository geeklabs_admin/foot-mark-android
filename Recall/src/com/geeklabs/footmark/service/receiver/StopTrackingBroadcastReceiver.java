package com.geeklabs.footmark.service.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.geeklabs.footmark.service.TrackingService;
import com.geeklabs.footmark.util.TrackingServiceManager;

public class StopTrackingBroadcastReceiver extends BroadcastReceiver {

	 @Override
	   public void onReceive(Context context, Intent intent) {
		 Log.i("FM: stop track System event type", intent.getAction());
		 if (TrackingServiceManager.canContinue(context)) {
			 Intent i = new Intent(context, TrackingService.class);
			 AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			 
			 PendingIntent pIntent = PendingIntent.getService(context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
	
			 // Stop scheduler
			 alarm.cancel(pIntent);
			 
			 // Stop service
			 context.stopService(i);
			 
			// Set tracking status
			TrackingServiceManager.setTrackingStatusToPaused(context);
		 }
	 }
}
