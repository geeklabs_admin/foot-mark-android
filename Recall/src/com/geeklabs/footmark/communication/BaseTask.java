package com.geeklabs.footmark.communication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.geeklabs.footmark.preferences.AuthPreferences;
import com.geeklabs.footmark.util.NetworkService;

public abstract class BaseTask extends
		AsyncTask<JSONObject, ProgressDialog, String> {

	protected ProgressDialog progressDialog;
	private AuthPreferences authPreferences;
	private Context context;
	protected boolean isNetworkAvailable = true;

	public BaseTask(ProgressDialog progressDialog, Context context) {
		this.progressDialog = progressDialog;
		this.context = context;
		authPreferences = new AuthPreferences(context);
	}

	@Override
	protected void onPreExecute() {
		if (!NetworkService.isNetWorkAvailable(context)) {
			isNetworkAvailable = false;
		}

		// Show progress if available
		if (progressDialog != null) {
			progressDialog.show();
		}
	}

	@Override
	protected void onPostExecute(String result) {
		// Handle response
		try {			
			if (result != null) {
				try {
					handleResponse(result);
				} catch (JSONException e) {
					Log.e(" JSONException ", "JSONException while handling response " + e.getMessage());
					e.printStackTrace();
				}
			}
		} finally { // We should close dialog if exist
			cancelDialog();			
		}
	}

	@Override
	protected String doInBackground(JSONObject... params) {
		String jsonOutput = "";

		if (!NetworkService.isNetWorkAvailable(context)) {
			return executeTaskInOffline();
		}

		// Form request url with required http parameters for every request
		String requestUrl = getRequestUrl() + "?" + getMandatoryRequestParams();
		
		HttpURLConnection conn = null;  
		try {
			URL url = new URL(requestUrl);
			conn = (HttpURLConnection) url.openConnection();
			
			// set request connection properties
			setConnectProperties(conn); 
			
			// Child class should override it
			doRequestSpecificTask(conn);
			
			// Fire request and get response :)
			conn.connect();
			
			int response = conn.getResponseCode();
			if (response == HttpURLConnection.HTTP_OK) {
				Log.d("Base URL", "The response is: " + response);
				// Convert the InputStream into a string
				jsonOutput = readIt(conn.getInputStream());
			}
		} catch (MalformedURLException e) {
			Log.e(" URL Issue ", "Url is invalid: " + e.getMessage());
			e.printStackTrace();
		} catch (ProtocolException e) {
			Log.e(" ProtocolException ", "ProtocolException: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			Log.e(" IOException ", "IOException: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
		return jsonOutput;
	}

	protected abstract void doRequestSpecificTask(HttpURLConnection conn) throws ProtocolException, IOException;

	protected abstract String getRequestUrl();

	protected abstract void showMessageOnUI(String string);

	protected abstract void handleResponse(String jsonResponse) throws JSONException;

	protected String executeTaskInOffline() {
		return null;
	}

	protected void addExtraParams(List<NameValuePair> params) {
		// By default won't add any extra params
	}
	
	private String readIt(InputStream is) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(is, "utf-8"));
		String line = null;
		StringBuilder respContent = new StringBuilder();
		while ((line = br.readLine()) != null) {
			respContent.append(line);
		}
		br.close();
		return respContent.toString();
	}

	private String getMandatoryRequestParams() {
		List<NameValuePair> params = new LinkedList<NameValuePair>();
		params.add(new BasicNameValuePair("userId", String
				.valueOf(authPreferences.getUserId())));
		params.add(new BasicNameValuePair("userUid", authPreferences
				.getUserUid()));

		// Child can add some more specific params
		addExtraParams(params);
				
		return URLEncodedUtils.format(params, "utf-8");
	}

	private void setConnectProperties(HttpURLConnection conn) {
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setRequestProperty("Accept", "application/json");
		conn.setRequestProperty("charset", "utf-8");
		
		// Set time out properties
		conn.setReadTimeout(15000 /* milliseconds */);
		conn.setConnectTimeout(30000 /* milliseconds */);
		
		conn.setInstanceFollowRedirects(false);
	}
	
	protected void cancelDialog() {
		if (progressDialog != null) {
			progressDialog.cancel();
		}
	}
}
