package com.geeklabs.footmark.communication.post.task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.JSONException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;

import com.geeklabs.footmark.activity.HomeFragment;
import com.geeklabs.footmark.communication.AbstractHttpPostTask;
import com.geeklabs.footmark.domain.OfflineTrack;
import com.geeklabs.footmark.domain.SyncedTrack;
import com.geeklabs.footmark.preferences.AuthPreferences;
import com.geeklabs.footmark.sqllite.SQLLiteManager;
import com.geeklabs.footmark.util.RequestUrl;

public class SendTrackTask extends AbstractHttpPostTask {
	private final Context context;
	private final AuthPreferences authPreferences;
	private Activity activity;

	public SendTrackTask(Context context, ProgressDialog progressDialog) {
		super(progressDialog, context);
		this.context = context;
//		this.activity = (Activity) context;
		authPreferences = new AuthPreferences(context);
	}

	@Override
	protected void showMessageOnUI(String string) {
		// NO need to show anything back to user on UI, as it is back ground service
	}

	@Override
	protected String getRequestUrl() {
		return RequestUrl.SEND_TRACKS.replace("{id}", String.valueOf(authPreferences.getUserId()));
	}

	@Override
	protected String getRequestJSON() {
		// Get trackings
		List<OfflineTrack> trackings = SQLLiteManager.getInstance(context).getAllOfflineTrackings();

		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(trackings);
		} catch (Exception e) {
			throw new IllegalStateException("problem occured json parsing while sending tracks");
		}
	}

	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {

		if (!jsonResponse.isEmpty()) {
			// response is success, then remove tracks from db
			ObjectMapper mapper = new ObjectMapper();
			List<SyncedTrack> tracks = new ArrayList<SyncedTrack>();
			// get all trackings from response
			try {
				tracks = mapper.readValue(jsonResponse, new TypeReference<List<SyncedTrack>>() {
				});
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			//save Synced Tracks
			for (SyncedTrack syncedTrack : tracks) {
				SQLLiteManager.getInstance(context).saveSyncedTrack(syncedTrack);
			}
			
			// Delete - Need to delete only synced tracks so we dont remove new tracks saved in the gap.
			SQLLiteManager.getInstance(context).deleteAllOfflineTrackings();
			
			
			/*Intent intent = new Intent(context, HomeFragment.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//			activity.finish();
			context.startActivity(intent);*/
		}
	}
}