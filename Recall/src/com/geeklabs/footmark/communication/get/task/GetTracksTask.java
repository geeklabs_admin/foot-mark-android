package com.geeklabs.footmark.communication.get.task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.JSONException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;
import android.widget.Toast;

import com.geeklabs.footmark.communication.AbstractHttpGetTask;
import com.geeklabs.footmark.domain.SyncedTrack;

public abstract class GetTracksTask extends AbstractHttpGetTask {

	private Activity contextActivity;
	private int currentPage;

	public GetTracksTask(int currentPage, Activity context, ProgressDialog getTracksProgressDialog) {
		super(getTracksProgressDialog, context);
		this.currentPage = currentPage;
		this.contextActivity = context;
	}

	@Override
	protected void showMessageOnUI(final String message) {
		contextActivity.runOnUiThread(new Runnable() {
			public void run() {
				Toast.makeText(contextActivity, message,
						Toast.LENGTH_SHORT).show();
				Log.i("Loading ...", message);
			}
		});
	}
	
	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		try {
			List<SyncedTrack> tracks = new ArrayList<SyncedTrack>();
			if (!jsonResponse.isEmpty()) {
				ObjectMapper mapper = new ObjectMapper();
				tracks = mapper.readValue(jsonResponse, new TypeReference<List<SyncedTrack>>(){});
			}
			updateUI(tracks);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {			
			cancelDialog();
		}
	}

	protected abstract void updateUI(List<SyncedTrack> tracks);

	@Override
	protected void addExtraParams(List<NameValuePair> params) {
		params.add(new BasicNameValuePair("currentPage", String.valueOf(currentPage)));
	}
	
	@Override
	protected String getRequestUrl() {
		return"";
	}
}
