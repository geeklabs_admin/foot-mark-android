package com.geeklabs.footmark.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.geeklabs.footmark.R;
import com.geeklabs.footmark.communication.post.task.PreferencesTask;
import com.geeklabs.footmark.domain.Preferences;
import com.geeklabs.footmark.preferences.UserPreferences;

public class SettingsActivity extends Activity implements OnItemSelectedListener {

	private SpinnerSelection distanceSelector;
	private SpinnerSelection timeSelector;
	private UserPreferences userPreferences;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);

		userPreferences = new UserPreferences(getApplicationContext());
		
		// Action Bar color
		BitmapDrawable bitmap = (BitmapDrawable) getResources().getDrawable(R.drawable.steel_blue);
		bitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
		getActionBar().setBackgroundDrawable(bitmap);

		// back to Home
		getActionBar().setDisplayHomeAsUpEnabled(true);

		addDistanceRadioButtons();
		addTimeRadioButtons();
	}

	private class SpinnerSelection implements OnItemSelectedListener {
		private Object selectedItem;

		@Override
		public void onItemSelected(AdapterView<?> view, View arg1, int arg2, long arg3) {
			selectedItem = view.getSelectedItem();
		}
		
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
		}
		
		public Object getSelectedItem() {
			return selectedItem;
		}
	}
	
	public static void selectSpinnerItemByValue(Spinner spnr, String value) {
		@SuppressWarnings("unchecked")
		ArrayAdapter<String> adapter = (ArrayAdapter<String>) spnr.getAdapter();
	    for (int position = 0; position < adapter.getCount(); position++) {
	        if(adapter.getItem(position).equals(value)) {
	            spnr.setSelection(position);
	            return;
	        }
	    }
	}
	
	public void addDistanceRadioButtons() {
		// Spinner element
		Spinner spinner1 = (Spinner) findViewById(R.id.distance_spinner);
		
		distanceSelector = new SpinnerSelection();
		spinner1.setOnItemSelectedListener(distanceSelector);

		// Spinner Drop down elements
		List<String> categories = new ArrayList<String>();
		categories.add("1");
		categories.add("2");
		categories.add("3");
		categories.add("4");
		categories.add("5");
		categories.add("10");
		categories.add("25");
		categories.add("50");
		categories.add("100");

		// Creating adapter for spinner
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(SettingsActivity.this,
				android.R.layout.simple_spinner_item, categories);

		// Drop down layout style - list view with radio button
		dataAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);

		// attaching data adapter to spinner
		spinner1.setAdapter(dataAdapter);
		
		final int distance = (int) userPreferences.getDistance();
		selectSpinnerItemByValue(spinner1, String.valueOf(distance));
	}

	public void addTimeRadioButtons() {
		// Spinner element
		Spinner spinner = (Spinner) findViewById(R.id.time_spinner);

		timeSelector = new SpinnerSelection();
		spinner.setOnItemSelectedListener(timeSelector);

		// Spinner Drop down elements
		List<String> categories = new ArrayList<String>();
		categories.add("3");
		categories.add("5");
		categories.add("10");
		categories.add("15");
		categories.add("20");

		// Creating adapter for spinner
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
				categories);

		// Drop down layout style - list view with radio button
		dataAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);

		// attaching data adapter to spinner
		spinner.setAdapter(dataAdapter);
		final int time =  userPreferences.getTime();
		selectSpinnerItemByValue(spinner, String.valueOf(time));
		
	}

	private void saveUserSettingPreferences() {

		final ProgressDialog userPrefAPKProgressDialog = new ProgressDialog(SettingsActivity.this);
		userPrefAPKProgressDialog.setMessage("Updating your preferences, please wait...");
		userPrefAPKProgressDialog.setCancelable(false);

		// Show progress bar to user
		userPrefAPKProgressDialog.show();

		Preferences preferences = new Preferences();
		preferences.setDistance(Float.parseFloat((String)distanceSelector.getSelectedItem()));
		preferences.setTime(Integer.parseInt((String)timeSelector.getSelectedItem()));
		
		PreferencesTask userPreferencesTask = new PreferencesTask(SettingsActivity.this, userPrefAPKProgressDialog, 
				preferences);
		userPreferencesTask.execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add("save").setIcon(R.drawable.save).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		}

		if (item.getTitle().equals("save")) {
			saveUserSettingPreferences();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		parent.getItemAtPosition(position).toString();
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {

	}

}
