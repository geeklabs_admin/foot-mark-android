package com.geeklabs.footmark.util;

public final class BroadcastEvent {

	private BroadcastEvent() {}
	
	public static final String START_TRACK_SERVICE = "com.geeklabs.footmark.START_TRACK_SERVICE";
	public static final String RESUME_TRACK_SERVICE = "com.geeklabs.footmark.RESUME_TRACK_SERVICE";
	public static final String STOP_TRACK_SERVICE = "com.geeklabs.footmark.STOP_TRACK_SERVICE";
}
