package com.geeklabs.footmark.communication.get.task;

import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.geeklabs.footmark.activity.FootMarkActivity;
import com.geeklabs.footmark.communication.AbstractHttpGetTask;
import com.geeklabs.footmark.preferences.AuthPreferences;
import com.geeklabs.footmark.preferences.UserPreferences;
import com.geeklabs.footmark.util.RequestUrl;
import com.geeklabs.footmark.util.TrackingServiceManager;

public class SignoutTask extends AbstractHttpGetTask {
	private UserPreferences userPreferences;
	private AuthPreferences authPreferences;
	private Activity contextActivity;

	public SignoutTask(Activity context, ProgressDialog signoutProgressDialog) {
		super(signoutProgressDialog, context);
		this.contextActivity = context;
		authPreferences = new AuthPreferences(context);
		userPreferences = new UserPreferences(context);
	}
	
	@Override
	protected void showMessageOnUI(final String message) {
		contextActivity.runOnUiThread(new Runnable() {
			public void run() {
				Toast.makeText(contextActivity, message,
						Toast.LENGTH_SHORT).show();
				Log.i("FM Signout Processing", message);
			}
		});
	}
	
	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		JSONObject jsonResObj = new JSONObject(jsonResponse);
		String status = jsonResObj.getString("status");
		if ("success".equals(status)) {
			// Stop tracking service
			TrackingServiceManager.stopTracking(contextActivity);
			
			// Request signout to Google server
			singoutUser();
			
			// Set user sign in status 
			authPreferences.setSignInStatus(false);

			// User id
			authPreferences.setUserId(0);
			
			//set user_uuid
			authPreferences.setUserUID("");
			
			// Update User pref
			userPreferences.clearValues();
			
			Intent i = new Intent(contextActivity, FootMarkActivity.class);
			contextActivity.startActivity(i);
			
			showMessageOnUI(" Your successfully logged out");
			cancelDialog();
		} else {
			showMessageOnUI("Unable to reach server, try after sometime");
			cancelDialog();
		}
	}

	private void singoutUser() {
		AccountManager accountManager = AccountManager.get(contextActivity);
		accountManager.invalidateAuthToken("com.google", authPreferences.getAccessToken());
 
		authPreferences.setAccessToken(null);
	}
	
	@Override
	protected String getRequestUrl() {
		return RequestUrl.SIGNOUT_REQ.replace("{id}", String.valueOf(authPreferences.getUserId()));
	}
}
