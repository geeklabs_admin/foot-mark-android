package com.geeklabs.footmark.event;

import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;

/**
 * Taken from http://benjii.me/2010/08/endless-scrolling-listview-in-android/
 */
public abstract class EndlessScrollListener implements OnScrollListener {

	// The minimum amount of items to have below your current scroll position, before loading more.
    private int visibleThreshold = 10;
    private int currentPage = 0;
    private int previousTotal = 0;
    private boolean loading = true;

    public EndlessScrollListener() {
    }
    public EndlessScrollListener(int visibleThreshold) {
        this.visibleThreshold = visibleThreshold;
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false;
                previousTotal = totalItemCount;
                currentPage++;
            }
        }
        if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
            runTask(currentPage);
            loading = true;
        }
    }

    protected abstract void runTask(int currentPage);
    
	@Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }
}