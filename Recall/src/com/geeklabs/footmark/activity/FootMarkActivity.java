package com.geeklabs.footmark.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.Settings;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.geeklabs.footmark.R;
import com.geeklabs.footmark.oauth.AuthActivity;
import com.geeklabs.footmark.preferences.AuthPreferences;
import com.geeklabs.footmark.util.NetworkService;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;

public class FootMarkActivity extends Activity implements AnimationListener {
	private AuthPreferences authPreferences;
	Animation animFadein;
	private static final int GPS_NW_SEETINGS_CODE = 2014;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		authPreferences = new AuthPreferences(this);
		setContentView(R.layout.home);

		final TextView description = (TextView) findViewById(R.id.decription);
		final SignInButton sign_in_button = (SignInButton) findViewById(R.id.sign_in_button);
		final ImageView logo = (ImageView) findViewById(R.id.logo);

		// Hide views on image
		hideViews();

		// load the animation
		animFadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);

		// set animation listener
		animFadein.setAnimationListener(this);

		// display the next image during 2 seconds
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				showViews();
				findViewById(R.id.sign_in_button).setOnClickListener(
						new OnClickListener() {
							@Override
							public void onClick(View v) {
								LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
								// Check for n/w connection
								if (!NetworkService.isNetWorkAvailable(FootMarkActivity.this)) {
									AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(FootMarkActivity.this);
									// set title
									alertDialogBuilder.setTitle("Warning");
									alertDialogBuilder
											.setMessage("Check your network connection and try again.");
									// set dialog message
									alertDialogBuilder
											.setCancelable(false)
											.setNegativeButton(
													"Ok",
													new DialogInterface.OnClickListener() {
														public void onClick(DialogInterface dialog, int id) {
														}
													});
									// create alert dialog
									AlertDialog alertDialog = alertDialogBuilder
											.create();

									// show it
									alertDialog.show();
									
									//check user really enable or not
									
								} else if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext()) == 0 && locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)/* || locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)*/) {
										Intent i = new Intent(getApplicationContext(), AuthActivity.class);
										startActivity(i);
										
									}
									 else {
										// check setup for locations
										String recheckLocationSetup = isLocationSetupReady();
										isLocationSetupDone(recheckLocationSetup);

								}
							}
						});
			}

			private void showViews() {
				sign_in_button.startAnimation(animFadein);
				description.startAnimation(animFadein);
				sign_in_button.setVisibility(View.VISIBLE);
				description.setVisibility(View.VISIBLE);
				logo.setVisibility(View.INVISIBLE);

			}
		}, 2000);

		if (authPreferences.isUserSignedIn() && authPreferences.getUserAccount() != null) {
			Intent i = new Intent(getApplicationContext(), HomeFragment.class);
			startActivity(i);
		}

		setStrictMode();
	}

	private void hideViews() {
		final TextView description = (TextView) findViewById(R.id.decription);
		final SignInButton sign_in_button = (SignInButton) findViewById(R.id.sign_in_button);
		// final ImageView logo = (ImageView) findViewById(R.id.logo);

		sign_in_button.setVisibility(View.INVISIBLE);
		description.setVisibility(View.INVISIBLE);
		// logo.setVisibility(View.GONE);

	}

	private void setStrictMode() {
		if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
	}

	@Override
	public void onAnimationEnd(Animation animation) {
	}

	@Override
	public void onAnimationRepeat(Animation animation) {
	}

	@Override
	public void onAnimationStart(Animation animation) {
	}

	@Override
	public void onBackPressed() {
		moveTaskToBack(true);
	}

	private String isLocationSetupReady() {

		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
		LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

		if (resultCode != 0) {
			return "google play";
		} else if (!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) || !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			return "use n/w wire less";
		}
		return null;
	}

	private void isLocationSetupDone(String locationSetup) {

		if (locationSetup != null) {

			if ("google play".equals(locationSetup)) {
			
				// show message for google settings
				// Build the alert dialog
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle("Google Play Services Not Avilable");
				builder.setMessage("Please Install Google Play Services");
				builder.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialogInterface, int i) {
								// no action
								Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.gms&hl=en"));
								startActivity(intent);
							}
						});
				Dialog alertDialog = builder.create();
				alertDialog.setCanceledOnTouchOutside(false);
				alertDialog.show();
				
} else if ("use n/w wire less".equals(locationSetup)) {
			
				// Build the alert dialog
				AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
				builder1.setTitle("Location Services Not Active");
				builder1.setMessage("Please enable Location Services and GPS");
				builder1.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(
									DialogInterface dialogInterface, int i) {
								// Show location settings when the user
								// acknowledges
								// the alert dialog

								Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
								startActivityForResult(intent,GPS_NW_SEETINGS_CODE);

							}
						});
				Dialog alertDialog1 = builder1.create();
				alertDialog1.setCanceledOnTouchOutside(false);
				alertDialog1.show();
				
				
				}
			}

		}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		isUserNetworkOrGpsEnabled();
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void isUserNetworkOrGpsEnabled() {
		 LocationManager locationManager  = (LocationManager) getSystemService(LOCATION_SERVICE);
		if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) /*|| locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)*/){
			Intent i = new Intent(getApplicationContext(), AuthActivity.class);
			startActivity(i);
		}
	}
	
}
